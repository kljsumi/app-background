package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.AllIndex;
import com.ruoyi.data.mapper.AllIndexMapper;
import com.ruoyi.data.service.IAllIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 索引Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class AllIndexServiceImpl implements IAllIndexService 
{
    @Autowired
    private AllIndexMapper allIndexMapper;

    /**
     * 查询索引
     * 
     * @param id 索引主键
     * @return 索引
     */
    @Override
    public AllIndex selectAllIndexById(Long id)
    {
        return allIndexMapper.selectAllIndexById(id);
    }

    /**
     * 查询索引列表
     * 
     * @param allIndex 索引
     * @return 索引
     */
    @Override
    public List<AllIndex> selectAllIndexList(AllIndex allIndex)
    {
        return allIndexMapper.selectAllIndexList(allIndex);
    }

    /**
     * 新增索引
     * 
     * @param allIndex 索引
     * @return 结果
     */
    @Override
    public int insertAllIndex(AllIndex allIndex)
    {
        return allIndexMapper.insertAllIndex(allIndex);
    }

    /**
     * 修改索引
     * 
     * @param allIndex 索引
     * @return 结果
     */
    @Override
    public int updateAllIndex(AllIndex allIndex)
    {
        return allIndexMapper.updateAllIndex(allIndex);
    }

    /**
     * 批量删除索引
     * 
     * @param ids 需要删除的索引主键
     * @return 结果
     */
    @Override
    public int deleteAllIndexByIds(Long[] ids)
    {
        return allIndexMapper.deleteAllIndexByIds(ids);
    }

    /**
     * 删除索引信息
     * 
     * @param id 索引主键
     * @return 结果
     */
    @Override
    public int deleteAllIndexById(Long id)
    {
        return allIndexMapper.deleteAllIndexById(id);
    }

    @Override
    public List<AllIndex> getIndexByListName(String listName) {
        return allIndexMapper.getIndexByName(listName);
    }
}
