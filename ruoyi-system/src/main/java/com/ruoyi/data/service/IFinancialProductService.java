package com.ruoyi.data.service;

import com.ruoyi.data.domain.FinancialProduct;

import java.util.List;

/**
 * 金融产品Service接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface IFinancialProductService 
{
    /**
     * 查询金融产品
     * 
     * @param financialProductId 金融产品主键
     * @return 金融产品
     */
    public FinancialProduct selectFinancialProductByFinancialProductId(Long financialProductId);

    /**
     * 查询金融产品列表
     * 
     * @param financialProduct 金融产品
     * @return 金融产品集合
     */
    public List<FinancialProduct> selectFinancialProductList(FinancialProduct financialProduct);

    /**
     * 新增金融产品
     * 
     * @param financialProduct 金融产品
     * @return 结果
     */
    public int insertFinancialProduct(FinancialProduct financialProduct);

    /**
     * 修改金融产品
     * 
     * @param financialProduct 金融产品
     * @return 结果
     */
    public int updateFinancialProduct(FinancialProduct financialProduct);

    /**
     * 批量删除金融产品
     * 
     * @param financialProductIds 需要删除的金融产品主键集合
     * @return 结果
     */
    public int deleteFinancialProductByFinancialProductIds(Long[] financialProductIds);

    /**
     * 删除金融产品信息
     * 
     * @param financialProductId 金融产品主键
     * @return 结果
     */
    public int deleteFinancialProductByFinancialProductId(Long financialProductId);
}
