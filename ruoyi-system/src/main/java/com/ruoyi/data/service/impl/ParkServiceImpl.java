package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.Park;
import com.ruoyi.data.mapper.ParkMapper;
import com.ruoyi.data.service.IParkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 科技园区Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class ParkServiceImpl implements IParkService 
{
    @Autowired
    private ParkMapper parkMapper;

    /**
     * 查询科技园区
     * 
     * @param parkId 科技园区主键
     * @return 科技园区
     */
    @Override
    public Park selectParkByParkId(Long parkId)
    {
        return parkMapper.selectParkByParkId(parkId);
    }

    /**
     * 查询科技园区列表
     * 
     * @param park 科技园区
     * @return 科技园区
     */
    @Override
    public List<Park> selectParkList(Park park)
    {
        return parkMapper.selectParkList(park);
    }

    /**
     * 新增科技园区
     * 
     * @param park 科技园区
     * @return 结果
     */
    @Override
    public int insertPark(Park park)
    {
        return parkMapper.insertPark(park);
    }

    /**
     * 修改科技园区
     * 
     * @param park 科技园区
     * @return 结果
     */
    @Override
    public int updatePark(Park park)
    {
        return parkMapper.updatePark(park);
    }

    /**
     * 批量删除科技园区
     * 
     * @param parkIds 需要删除的科技园区主键
     * @return 结果
     */
    @Override
    public int deleteParkByParkIds(Long[] parkIds)
    {
        return parkMapper.deleteParkByParkIds(parkIds);
    }

    /**
     * 删除科技园区信息
     * 
     * @param parkId 科技园区主键
     * @return 结果
     */
    @Override
    public int deleteParkByParkId(Long parkId)
    {
        return parkMapper.deleteParkByParkId(parkId);
    }
}
