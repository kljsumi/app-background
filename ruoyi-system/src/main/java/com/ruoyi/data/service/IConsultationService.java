package com.ruoyi.data.service;

import com.ruoyi.data.domain.Consultation;

import java.util.List;

/**
 * 咨询Service接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface IConsultationService 
{
    /**
     * 查询咨询
     * 
     * @param id 咨询主键
     * @return 咨询
     */
    public Consultation selectConsultationById(Long id);

    /**
     * 查询咨询列表
     * 
     * @param consultation 咨询
     * @return 咨询集合
     */
    public List<Consultation> selectConsultationList(Consultation consultation);

    /**
     * 新增咨询
     * 
     * @param consultation 咨询
     * @return 结果
     */
    public int insertConsultation(Consultation consultation);

    /**
     * 修改咨询
     * 
     * @param consultation 咨询
     * @return 结果
     */
    public int updateConsultation(Consultation consultation);

    /**
     * 批量删除咨询
     * 
     * @param ids 需要删除的咨询主键集合
     * @return 结果
     */
    public int deleteConsultationByIds(Long[] ids);

    /**
     * 删除咨询信息
     * 
     * @param id 咨询主键
     * @return 结果
     */
    public int deleteConsultationById(Long id);
}
