package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.TechnologyNeed;
import com.ruoyi.data.mapper.TechnologyNeedMapper;
import com.ruoyi.data.service.ITechnologyNeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 技术需求Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class TechnologyNeedServiceImpl implements ITechnologyNeedService 
{
    @Autowired
    private TechnologyNeedMapper technologyNeedMapper;

    /**
     * 查询技术需求
     * 
     * @param technologyNeedId 技术需求主键
     * @return 技术需求
     */
    @Override
    public TechnologyNeed selectTechnologyNeedByTechnologyNeedId(Long technologyNeedId)
    {
        return technologyNeedMapper.selectTechnologyNeedByTechnologyNeedId(technologyNeedId);
    }

    /**
     * 查询技术需求列表
     * 
     * @param technologyNeed 技术需求
     * @return 技术需求
     */
    @Override
    public List<TechnologyNeed> selectTechnologyNeedList(TechnologyNeed technologyNeed)
    {
        return technologyNeedMapper.selectTechnologyNeedList(technologyNeed);
    }

    /**
     * 新增技术需求
     * 
     * @param technologyNeed 技术需求
     * @return 结果
     */
    @Override
    public int insertTechnologyNeed(TechnologyNeed technologyNeed)
    {
        return technologyNeedMapper.insertTechnologyNeed(technologyNeed);
    }

    /**
     * 修改技术需求
     * 
     * @param technologyNeed 技术需求
     * @return 结果
     */
    @Override
    public int updateTechnologyNeed(TechnologyNeed technologyNeed)
    {
        return technologyNeedMapper.updateTechnologyNeed(technologyNeed);
    }

    /**
     * 批量删除技术需求
     * 
     * @param technologyNeedIds 需要删除的技术需求主键
     * @return 结果
     */
    @Override
    public int deleteTechnologyNeedByTechnologyNeedIds(Long[] technologyNeedIds)
    {
        return technologyNeedMapper.deleteTechnologyNeedByTechnologyNeedIds(technologyNeedIds);
    }

    /**
     * 删除技术需求信息
     * 
     * @param technologyNeedId 技术需求主键
     * @return 结果
     */
    @Override
    public int deleteTechnologyNeedByTechnologyNeedId(Long technologyNeedId)
    {
        return technologyNeedMapper.deleteTechnologyNeedByTechnologyNeedId(technologyNeedId);
    }
}
