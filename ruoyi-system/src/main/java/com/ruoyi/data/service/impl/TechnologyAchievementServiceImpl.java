package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.TechnologyAchievement;
import com.ruoyi.data.mapper.TechnologyAchievementMapper;
import com.ruoyi.data.service.ITechnologyAchievementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 技术成果Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class TechnologyAchievementServiceImpl implements ITechnologyAchievementService 
{
    @Autowired
    private TechnologyAchievementMapper technologyAchievementMapper;

    /**
     * 查询技术成果
     * 
     * @param technologyAchievementId 技术成果主键
     * @return 技术成果
     */
    @Override
    public TechnologyAchievement selectTechnologyAchievementByTechnologyAchievementId(Long technologyAchievementId)
    {
        return technologyAchievementMapper.selectTechnologyAchievementByTechnologyAchievementId(technologyAchievementId);
    }

    /**
     * 查询技术成果列表
     * 
     * @param technologyAchievement 技术成果
     * @return 技术成果
     */
    @Override
    public List<TechnologyAchievement> selectTechnologyAchievementList(TechnologyAchievement technologyAchievement)
    {
        return technologyAchievementMapper.selectTechnologyAchievementList(technologyAchievement);
    }

    /**
     * 新增技术成果
     * 
     * @param technologyAchievement 技术成果
     * @return 结果
     */
    @Override
    public int insertTechnologyAchievement(TechnologyAchievement technologyAchievement)
    {
        return technologyAchievementMapper.insertTechnologyAchievement(technologyAchievement);
    }

    /**
     * 修改技术成果
     * 
     * @param technologyAchievement 技术成果
     * @return 结果
     */
    @Override
    public int updateTechnologyAchievement(TechnologyAchievement technologyAchievement)
    {
        return technologyAchievementMapper.updateTechnologyAchievement(technologyAchievement);
    }

    /**
     * 批量删除技术成果
     * 
     * @param technologyAchievementIds 需要删除的技术成果主键
     * @return 结果
     */
    @Override
    public int deleteTechnologyAchievementByTechnologyAchievementIds(Long[] technologyAchievementIds)
    {
        return technologyAchievementMapper.deleteTechnologyAchievementByTechnologyAchievementIds(technologyAchievementIds);
    }

    /**
     * 删除技术成果信息
     * 
     * @param technologyAchievementId 技术成果主键
     * @return 结果
     */
    @Override
    public int deleteTechnologyAchievementByTechnologyAchievementId(Long technologyAchievementId)
    {
        return technologyAchievementMapper.deleteTechnologyAchievementByTechnologyAchievementId(technologyAchievementId);
    }
}
