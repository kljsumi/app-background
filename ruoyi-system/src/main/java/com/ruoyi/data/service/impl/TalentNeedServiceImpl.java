package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.TalentNeed;
import com.ruoyi.data.mapper.TalentNeedMapper;
import com.ruoyi.data.service.ITalentNeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 人才需求Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class TalentNeedServiceImpl implements ITalentNeedService 
{
    @Autowired
    private TalentNeedMapper talentNeedMapper;

    /**
     * 查询人才需求
     * 
     * @param talentNeedId 人才需求主键
     * @return 人才需求
     */
    @Override
    public TalentNeed selectTalentNeedByTalentNeedId(Long talentNeedId)
    {
        return talentNeedMapper.selectTalentNeedByTalentNeedId(talentNeedId);
    }

    /**
     * 查询人才需求列表
     * 
     * @param talentNeed 人才需求
     * @return 人才需求
     */
    @Override
    public List<TalentNeed> selectTalentNeedList(TalentNeed talentNeed)
    {
        return talentNeedMapper.selectTalentNeedList(talentNeed);
    }

    /**
     * 新增人才需求
     * 
     * @param talentNeed 人才需求
     * @return 结果
     */
    @Override
    public int insertTalentNeed(TalentNeed talentNeed)
    {
        return talentNeedMapper.insertTalentNeed(talentNeed);
    }

    /**
     * 修改人才需求
     * 
     * @param talentNeed 人才需求
     * @return 结果
     */
    @Override
    public int updateTalentNeed(TalentNeed talentNeed)
    {
        return talentNeedMapper.updateTalentNeed(talentNeed);
    }

    /**
     * 批量删除人才需求
     * 
     * @param talentNeedIds 需要删除的人才需求主键
     * @return 结果
     */
    @Override
    public int deleteTalentNeedByTalentNeedIds(Long[] talentNeedIds)
    {
        return talentNeedMapper.deleteTalentNeedByTalentNeedIds(talentNeedIds);
    }

    /**
     * 删除人才需求信息
     * 
     * @param talentNeedId 人才需求主键
     * @return 结果
     */
    @Override
    public int deleteTalentNeedByTalentNeedId(Long talentNeedId)
    {
        return talentNeedMapper.deleteTalentNeedByTalentNeedId(talentNeedId);
    }
}
