package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.Consultation;
import com.ruoyi.data.mapper.ConsultationMapper;
import com.ruoyi.data.service.IConsultationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 咨询Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class ConsultationServiceImpl implements IConsultationService 
{
    @Autowired
    private ConsultationMapper consultationMapper;

    /**
     * 查询咨询
     * 
     * @param id 咨询主键
     * @return 咨询
     */
    @Override
    public Consultation selectConsultationById(Long id)
    {
        return consultationMapper.selectConsultationById(id);
    }

    /**
     * 查询咨询列表
     * 
     * @param consultation 咨询
     * @return 咨询
     */
    @Override
    public List<Consultation> selectConsultationList(Consultation consultation)
    {
        return consultationMapper.selectConsultationList(consultation);
    }

    /**
     * 新增咨询
     * 
     * @param consultation 咨询
     * @return 结果
     */
    @Override
    public int insertConsultation(Consultation consultation)
    {
        return consultationMapper.insertConsultation(consultation);
    }

    /**
     * 修改咨询
     * 
     * @param consultation 咨询
     * @return 结果
     */
    @Override
    public int updateConsultation(Consultation consultation)
    {
        return consultationMapper.updateConsultation(consultation);
    }

    /**
     * 批量删除咨询
     * 
     * @param ids 需要删除的咨询主键
     * @return 结果
     */
    @Override
    public int deleteConsultationByIds(Long[] ids)
    {
        return consultationMapper.deleteConsultationByIds(ids);
    }

    /**
     * 删除咨询信息
     * 
     * @param id 咨询主键
     * @return 结果
     */
    @Override
    public int deleteConsultationById(Long id)
    {
        return consultationMapper.deleteConsultationById(id);
    }
}
