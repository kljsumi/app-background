package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.ServiceProduct;
import com.ruoyi.data.mapper.ServiceProductMapper;
import com.ruoyi.data.service.IServiceProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 服务产品Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class ServiceProductServiceImpl implements IServiceProductService 
{
    @Autowired
    private ServiceProductMapper serviceProductMapper;

    /**
     * 查询服务产品
     * 
     * @param serviceProductId 服务产品主键
     * @return 服务产品
     */
    @Override
    public ServiceProduct selectServiceProductByServiceProductId(Long serviceProductId)
    {
        return serviceProductMapper.selectServiceProductByServiceProductId(serviceProductId);
    }

    /**
     * 查询服务产品列表
     * 
     * @param serviceProduct 服务产品
     * @return 服务产品
     */
    @Override
    public List<ServiceProduct> selectServiceProductList(ServiceProduct serviceProduct)
    {
        return serviceProductMapper.selectServiceProductList(serviceProduct);
    }

    /**
     * 新增服务产品
     * 
     * @param serviceProduct 服务产品
     * @return 结果
     */
    @Override
    public int insertServiceProduct(ServiceProduct serviceProduct)
    {
        return serviceProductMapper.insertServiceProduct(serviceProduct);
    }

    /**
     * 修改服务产品
     * 
     * @param serviceProduct 服务产品
     * @return 结果
     */
    @Override
    public int updateServiceProduct(ServiceProduct serviceProduct)
    {
        return serviceProductMapper.updateServiceProduct(serviceProduct);
    }

    /**
     * 批量删除服务产品
     * 
     * @param serviceProductIds 需要删除的服务产品主键
     * @return 结果
     */
    @Override
    public int deleteServiceProductByServiceProductIds(Long[] serviceProductIds)
    {
        return serviceProductMapper.deleteServiceProductByServiceProductIds(serviceProductIds);
    }

    /**
     * 删除服务产品信息
     * 
     * @param serviceProductId 服务产品主键
     * @return 结果
     */
    @Override
    public int deleteServiceProductByServiceProductId(Long serviceProductId)
    {
        return serviceProductMapper.deleteServiceProductByServiceProductId(serviceProductId);
    }
}
