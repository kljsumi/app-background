package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.SchoolInfo;
import com.ruoyi.data.mapper.SchoolInfoMapper;
import com.ruoyi.data.service.ISchoolInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学校信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class SchoolInfoServiceImpl implements ISchoolInfoService 
{
    @Autowired
    private SchoolInfoMapper schoolInfoMapper;

    /**
     * 查询学校信息
     * 
     * @param schoolId 学校信息主键
     * @return 学校信息
     */
    @Override
    public SchoolInfo selectSchoolInfoBySchoolId(Long schoolId)
    {
        return schoolInfoMapper.selectSchoolInfoBySchoolId(schoolId);
    }

    /**
     * 查询学校信息列表
     * 
     * @param schoolInfo 学校信息
     * @return 学校信息
     */
    @Override
    public List<SchoolInfo> selectSchoolInfoList(SchoolInfo schoolInfo)
    {
        return schoolInfoMapper.selectSchoolInfoList(schoolInfo);
    }

    /**
     * 新增学校信息
     * 
     * @param schoolInfo 学校信息
     * @return 结果
     */
    @Override
    public int insertSchoolInfo(SchoolInfo schoolInfo)
    {
        return schoolInfoMapper.insertSchoolInfo(schoolInfo);
    }

    /**
     * 修改学校信息
     * 
     * @param schoolInfo 学校信息
     * @return 结果
     */
    @Override
    public int updateSchoolInfo(SchoolInfo schoolInfo)
    {
        return schoolInfoMapper.updateSchoolInfo(schoolInfo);
    }

    /**
     * 批量删除学校信息
     * 
     * @param schoolIds 需要删除的学校信息主键
     * @return 结果
     */
    @Override
    public int deleteSchoolInfoBySchoolIds(Long[] schoolIds)
    {
        return schoolInfoMapper.deleteSchoolInfoBySchoolIds(schoolIds);
    }

    /**
     * 删除学校信息信息
     * 
     * @param schoolId 学校信息主键
     * @return 结果
     */
    @Override
    public int deleteSchoolInfoBySchoolId(Long schoolId)
    {
        return schoolInfoMapper.deleteSchoolInfoBySchoolId(schoolId);
    }
}
