package com.ruoyi.data.service;

import com.ruoyi.data.domain.Park;

import java.util.List;

/**
 * 科技园区Service接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface IParkService 
{
    /**
     * 查询科技园区
     * 
     * @param parkId 科技园区主键
     * @return 科技园区
     */
    public Park selectParkByParkId(Long parkId);

    /**
     * 查询科技园区列表
     * 
     * @param park 科技园区
     * @return 科技园区集合
     */
    public List<Park> selectParkList(Park park);

    /**
     * 新增科技园区
     * 
     * @param park 科技园区
     * @return 结果
     */
    public int insertPark(Park park);

    /**
     * 修改科技园区
     * 
     * @param park 科技园区
     * @return 结果
     */
    public int updatePark(Park park);

    /**
     * 批量删除科技园区
     * 
     * @param parkIds 需要删除的科技园区主键集合
     * @return 结果
     */
    public int deleteParkByParkIds(Long[] parkIds);

    /**
     * 删除科技园区信息
     * 
     * @param parkId 科技园区主键
     * @return 结果
     */
    public int deleteParkByParkId(Long parkId);
}
