package com.ruoyi.data.service;

import com.ruoyi.data.domain.TechnologyNeed;

import java.util.List;

/**
 * 技术需求Service接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface ITechnologyNeedService 
{
    /**
     * 查询技术需求
     * 
     * @param technologyNeedId 技术需求主键
     * @return 技术需求
     */
    public TechnologyNeed selectTechnologyNeedByTechnologyNeedId(Long technologyNeedId);

    /**
     * 查询技术需求列表
     * 
     * @param technologyNeed 技术需求
     * @return 技术需求集合
     */
    public List<TechnologyNeed> selectTechnologyNeedList(TechnologyNeed technologyNeed);

    /**
     * 新增技术需求
     * 
     * @param technologyNeed 技术需求
     * @return 结果
     */
    public int insertTechnologyNeed(TechnologyNeed technologyNeed);

    /**
     * 修改技术需求
     * 
     * @param technologyNeed 技术需求
     * @return 结果
     */
    public int updateTechnologyNeed(TechnologyNeed technologyNeed);

    /**
     * 批量删除技术需求
     * 
     * @param technologyNeedIds 需要删除的技术需求主键集合
     * @return 结果
     */
    public int deleteTechnologyNeedByTechnologyNeedIds(Long[] technologyNeedIds);

    /**
     * 删除技术需求信息
     * 
     * @param technologyNeedId 技术需求主键
     * @return 结果
     */
    public int deleteTechnologyNeedByTechnologyNeedId(Long technologyNeedId);
}
