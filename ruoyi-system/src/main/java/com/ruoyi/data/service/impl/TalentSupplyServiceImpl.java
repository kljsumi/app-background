package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.TalentSupply;
import com.ruoyi.data.mapper.TalentSupplyMapper;
import com.ruoyi.data.service.ITalentSupplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 人才供应Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class TalentSupplyServiceImpl implements ITalentSupplyService 
{
    @Autowired
    private TalentSupplyMapper talentSupplyMapper;

    /**
     * 查询人才供应
     * 
     * @param talentSupplyId 人才供应主键
     * @return 人才供应
     */
    @Override
    public TalentSupply selectTalentSupplyByTalentSupplyId(Long talentSupplyId)
    {
        return talentSupplyMapper.selectTalentSupplyByTalentSupplyId(talentSupplyId);
    }

    /**
     * 查询人才供应列表
     * 
     * @param talentSupply 人才供应
     * @return 人才供应
     */
    @Override
    public List<TalentSupply> selectTalentSupplyList(TalentSupply talentSupply)
    {
        return talentSupplyMapper.selectTalentSupplyList(talentSupply);
    }

    /**
     * 新增人才供应
     * 
     * @param talentSupply 人才供应
     * @return 结果
     */
    @Override
    public int insertTalentSupply(TalentSupply talentSupply)
    {
        return talentSupplyMapper.insertTalentSupply(talentSupply);
    }

    /**
     * 修改人才供应
     * 
     * @param talentSupply 人才供应
     * @return 结果
     */
    @Override
    public int updateTalentSupply(TalentSupply talentSupply)
    {
        return talentSupplyMapper.updateTalentSupply(talentSupply);
    }

    /**
     * 批量删除人才供应
     * 
     * @param talentSupplyIds 需要删除的人才供应主键
     * @return 结果
     */
    @Override
    public int deleteTalentSupplyByTalentSupplyIds(Long[] talentSupplyIds)
    {
        return talentSupplyMapper.deleteTalentSupplyByTalentSupplyIds(talentSupplyIds);
    }

    /**
     * 删除人才供应信息
     * 
     * @param talentSupplyId 人才供应主键
     * @return 结果
     */
    @Override
    public int deleteTalentSupplyByTalentSupplyId(Long talentSupplyId)
    {
        return talentSupplyMapper.deleteTalentSupplyByTalentSupplyId(talentSupplyId);
    }
}
