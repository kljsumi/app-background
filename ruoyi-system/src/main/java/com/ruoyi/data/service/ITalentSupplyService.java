package com.ruoyi.data.service;

import com.ruoyi.data.domain.TalentSupply;

import java.util.List;

/**
 * 人才供应Service接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface ITalentSupplyService 
{
    /**
     * 查询人才供应
     * 
     * @param talentSupplyId 人才供应主键
     * @return 人才供应
     */
    public TalentSupply selectTalentSupplyByTalentSupplyId(Long talentSupplyId);

    /**
     * 查询人才供应列表
     * 
     * @param talentSupply 人才供应
     * @return 人才供应集合
     */
    public List<TalentSupply> selectTalentSupplyList(TalentSupply talentSupply);

    /**
     * 新增人才供应
     * 
     * @param talentSupply 人才供应
     * @return 结果
     */
    public int insertTalentSupply(TalentSupply talentSupply);

    /**
     * 修改人才供应
     * 
     * @param talentSupply 人才供应
     * @return 结果
     */
    public int updateTalentSupply(TalentSupply talentSupply);

    /**
     * 批量删除人才供应
     * 
     * @param talentSupplyIds 需要删除的人才供应主键集合
     * @return 结果
     */
    public int deleteTalentSupplyByTalentSupplyIds(Long[] talentSupplyIds);

    /**
     * 删除人才供应信息
     * 
     * @param talentSupplyId 人才供应主键
     * @return 结果
     */
    public int deleteTalentSupplyByTalentSupplyId(Long talentSupplyId);
}
