package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.FinancingNeed;
import com.ruoyi.data.mapper.FinancingNeedMapper;
import com.ruoyi.data.service.IFinancingNeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 融资需求Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class FinancingNeedServiceImpl implements IFinancingNeedService 
{
    @Autowired
    private FinancingNeedMapper financingNeedMapper;

    /**
     * 查询融资需求
     * 
     * @param financingNeedId 融资需求主键
     * @return 融资需求
     */
    @Override
    public FinancingNeed selectFinancingNeedByFinancingNeedId(Long financingNeedId)
    {
        return financingNeedMapper.selectFinancingNeedByFinancingNeedId(financingNeedId);
    }

    /**
     * 查询融资需求列表
     * 
     * @param financingNeed 融资需求
     * @return 融资需求
     */
    @Override
    public List<FinancingNeed> selectFinancingNeedList(FinancingNeed financingNeed)
    {
        return financingNeedMapper.selectFinancingNeedList(financingNeed);
    }

    /**
     * 新增融资需求
     * 
     * @param financingNeed 融资需求
     * @return 结果
     */
    @Override
    public int insertFinancingNeed(FinancingNeed financingNeed)
    {
        return financingNeedMapper.insertFinancingNeed(financingNeed);
    }

    /**
     * 修改融资需求
     * 
     * @param financingNeed 融资需求
     * @return 结果
     */
    @Override
    public int updateFinancingNeed(FinancingNeed financingNeed)
    {
        return financingNeedMapper.updateFinancingNeed(financingNeed);
    }

    /**
     * 批量删除融资需求
     * 
     * @param financingNeedIds 需要删除的融资需求主键
     * @return 结果
     */
    @Override
    public int deleteFinancingNeedByFinancingNeedIds(Long[] financingNeedIds)
    {
        return financingNeedMapper.deleteFinancingNeedByFinancingNeedIds(financingNeedIds);
    }

    /**
     * 删除融资需求信息
     * 
     * @param financingNeedId 融资需求主键
     * @return 结果
     */
    @Override
    public int deleteFinancingNeedByFinancingNeedId(Long financingNeedId)
    {
        return financingNeedMapper.deleteFinancingNeedByFinancingNeedId(financingNeedId);
    }
}
