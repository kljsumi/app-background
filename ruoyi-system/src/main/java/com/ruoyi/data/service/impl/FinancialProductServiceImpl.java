package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.FinancialProduct;
import com.ruoyi.data.mapper.FinancialProductMapper;
import com.ruoyi.data.service.IFinancialProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 金融产品Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class FinancialProductServiceImpl implements IFinancialProductService 
{
    @Autowired
    private FinancialProductMapper financialProductMapper;

    /**
     * 查询金融产品
     * 
     * @param financialProductId 金融产品主键
     * @return 金融产品
     */
    @Override
    public FinancialProduct selectFinancialProductByFinancialProductId(Long financialProductId)
    {
        return financialProductMapper.selectFinancialProductByFinancialProductId(financialProductId);
    }

    /**
     * 查询金融产品列表
     * 
     * @param financialProduct 金融产品
     * @return 金融产品
     */
    @Override
    public List<FinancialProduct> selectFinancialProductList(FinancialProduct financialProduct)
    {
        return financialProductMapper.selectFinancialProductList(financialProduct);
    }

    /**
     * 新增金融产品
     * 
     * @param financialProduct 金融产品
     * @return 结果
     */
    @Override
    public int insertFinancialProduct(FinancialProduct financialProduct)
    {
        return financialProductMapper.insertFinancialProduct(financialProduct);
    }

    /**
     * 修改金融产品
     * 
     * @param financialProduct 金融产品
     * @return 结果
     */
    @Override
    public int updateFinancialProduct(FinancialProduct financialProduct)
    {
        return financialProductMapper.updateFinancialProduct(financialProduct);
    }

    /**
     * 批量删除金融产品
     * 
     * @param financialProductIds 需要删除的金融产品主键
     * @return 结果
     */
    @Override
    public int deleteFinancialProductByFinancialProductIds(Long[] financialProductIds)
    {
        return financialProductMapper.deleteFinancialProductByFinancialProductIds(financialProductIds);
    }

    /**
     * 删除金融产品信息
     * 
     * @param financialProductId 金融产品主键
     * @return 结果
     */
    @Override
    public int deleteFinancialProductByFinancialProductId(Long financialProductId)
    {
        return financialProductMapper.deleteFinancialProductByFinancialProductId(financialProductId);
    }
}
