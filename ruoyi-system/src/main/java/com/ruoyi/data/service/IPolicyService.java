package com.ruoyi.data.service;

import com.ruoyi.data.domain.Policy;

import java.util.List;

/**
 * 政策Service接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface IPolicyService 
{
    /**
     * 查询政策
     * 
     * @param policyId 政策主键
     * @return 政策
     */
    public Policy selectPolicyByPolicyId(Long policyId);

    /**
     * 查询政策列表
     * 
     * @param policy 政策
     * @return 政策集合
     */
    public List<Policy> selectPolicyList(Policy policy);

    /**
     * 新增政策
     * 
     * @param policy 政策
     * @return 结果
     */
    public int insertPolicy(Policy policy);

    /**
     * 修改政策
     * 
     * @param policy 政策
     * @return 结果
     */
    public int updatePolicy(Policy policy);

    /**
     * 批量删除政策
     * 
     * @param policyIds 需要删除的政策主键集合
     * @return 结果
     */
    public int deletePolicyByPolicyIds(Long[] policyIds);

    /**
     * 删除政策信息
     * 
     * @param policyId 政策主键
     * @return 结果
     */
    public int deletePolicyByPolicyId(Long policyId);
}
