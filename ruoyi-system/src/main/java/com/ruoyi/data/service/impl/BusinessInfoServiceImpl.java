package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.BusinessInfo;
import com.ruoyi.data.mapper.BusinessInfoMapper;
import com.ruoyi.data.service.IBusinessInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 企业信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class BusinessInfoServiceImpl implements IBusinessInfoService 
{
    @Autowired
    private BusinessInfoMapper businessInfoMapper;

    /**
     * 查询企业信息
     * 
     * @param businessId 企业信息主键
     * @return 企业信息
     */
    @Override
    public BusinessInfo selectBusinessInfoByBusinessId(Long businessId)
    {
        return businessInfoMapper.selectBusinessInfoByBusinessId(businessId);
    }

    /**
     * 查询企业信息列表
     * 
     * @param businessInfo 企业信息
     * @return 企业信息
     */
    @Override
    public List<BusinessInfo> selectBusinessInfoList(BusinessInfo businessInfo)
    {
        return businessInfoMapper.selectBusinessInfoList(businessInfo);
    }

    /**
     * 新增企业信息
     * 
     * @param businessInfo 企业信息
     * @return 结果
     */
    @Override
    public int insertBusinessInfo(BusinessInfo businessInfo)
    {
        return businessInfoMapper.insertBusinessInfo(businessInfo);
    }

    /**
     * 修改企业信息
     * 
     * @param businessInfo 企业信息
     * @return 结果
     */
    @Override
    public int updateBusinessInfo(BusinessInfo businessInfo)
    {
        return businessInfoMapper.updateBusinessInfo(businessInfo);
    }

    /**
     * 批量删除企业信息
     * 
     * @param businessIds 需要删除的企业信息主键
     * @return 结果
     */
    @Override
    public int deleteBusinessInfoByBusinessIds(Long[] businessIds)
    {
        return businessInfoMapper.deleteBusinessInfoByBusinessIds(businessIds);
    }

    /**
     * 删除企业信息信息
     * 
     * @param businessId 企业信息主键
     * @return 结果
     */
    @Override
    public int deleteBusinessInfoByBusinessId(Long businessId)
    {
        return businessInfoMapper.deleteBusinessInfoByBusinessId(businessId);
    }
}
