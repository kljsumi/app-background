package com.ruoyi.data.service.impl;

import com.ruoyi.data.domain.Policy;
import com.ruoyi.data.mapper.PolicyMapper;
import com.ruoyi.data.service.IPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 政策Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@Service
public class PolicyServiceImpl implements IPolicyService 
{
    @Autowired
    private PolicyMapper policyMapper;

    /**
     * 查询政策
     * 
     * @param policyId 政策主键
     * @return 政策
     */
    @Override
    public Policy selectPolicyByPolicyId(Long policyId)
    {
        return policyMapper.selectPolicyByPolicyId(policyId);
    }

    /**
     * 查询政策列表
     * 
     * @param policy 政策
     * @return 政策
     */
    @Override
    public List<Policy> selectPolicyList(Policy policy)
    {
        return policyMapper.selectPolicyList(policy);
    }

    /**
     * 新增政策
     * 
     * @param policy 政策
     * @return 结果
     */
    @Override
    public int insertPolicy(Policy policy)
    {
        return policyMapper.insertPolicy(policy);
    }

    /**
     * 修改政策
     * 
     * @param policy 政策
     * @return 结果
     */
    @Override
    public int updatePolicy(Policy policy)
    {
        return policyMapper.updatePolicy(policy);
    }

    /**
     * 批量删除政策
     * 
     * @param policyIds 需要删除的政策主键
     * @return 结果
     */
    @Override
    public int deletePolicyByPolicyIds(Long[] policyIds)
    {
        return policyMapper.deletePolicyByPolicyIds(policyIds);
    }

    /**
     * 删除政策信息
     * 
     * @param policyId 政策主键
     * @return 结果
     */
    @Override
    public int deletePolicyByPolicyId(Long policyId)
    {
        return policyMapper.deletePolicyByPolicyId(policyId);
    }
}
