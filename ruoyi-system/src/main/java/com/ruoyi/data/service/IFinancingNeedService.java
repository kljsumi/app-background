package com.ruoyi.data.service;

import com.ruoyi.data.domain.FinancingNeed;

import java.util.List;

/**
 * 融资需求Service接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface IFinancingNeedService 
{
    /**
     * 查询融资需求
     * 
     * @param financingNeedId 融资需求主键
     * @return 融资需求
     */
    public FinancingNeed selectFinancingNeedByFinancingNeedId(Long financingNeedId);

    /**
     * 查询融资需求列表
     * 
     * @param financingNeed 融资需求
     * @return 融资需求集合
     */
    public List<FinancingNeed> selectFinancingNeedList(FinancingNeed financingNeed);

    /**
     * 新增融资需求
     * 
     * @param financingNeed 融资需求
     * @return 结果
     */
    public int insertFinancingNeed(FinancingNeed financingNeed);

    /**
     * 修改融资需求
     * 
     * @param financingNeed 融资需求
     * @return 结果
     */
    public int updateFinancingNeed(FinancingNeed financingNeed);

    /**
     * 批量删除融资需求
     * 
     * @param financingNeedIds 需要删除的融资需求主键集合
     * @return 结果
     */
    public int deleteFinancingNeedByFinancingNeedIds(Long[] financingNeedIds);

    /**
     * 删除融资需求信息
     * 
     * @param financingNeedId 融资需求主键
     * @return 结果
     */
    public int deleteFinancingNeedByFinancingNeedId(Long financingNeedId);
}
