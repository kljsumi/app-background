package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 技术需求对象 technology_need
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class TechnologyNeed extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long technologyNeedId;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 分类 */
    @Excel(name = "分类")
    private String category;

    /** 行业领域 */
    @Excel(name = "行业领域")
    private String field;

    /** 详情图片 */
    @Excel(name = "详情图片")
    private String image;

    /** 联系人id */
    @Excel(name = "联系人id")
    private Long contactId;

    /** 企业id */
    @Excel(name = "企业id")
    private Long businessId;

    /** 联系人姓名 */
    @Excel(name = "联系人姓名")
    private String contactName;

    /** 联系人电话 */
    @Excel(name = "联系人电话")
    private String contactPhone;

    /** 联系人邮箱 */
    @Excel(name = "联系人邮箱")
    private String contactEmail;

    public void setTechnologyNeedId(Long technologyNeedId) 
    {
        this.technologyNeedId = technologyNeedId;
    }

    public Long getTechnologyNeedId() 
    {
        return technologyNeedId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setField(String field) 
    {
        this.field = field;
    }

    public String getField() 
    {
        return field;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setContactId(Long contactId) 
    {
        this.contactId = contactId;
    }

    public Long getContactId() 
    {
        return contactId;
    }
    public void setBusinessId(Long businessId) 
    {
        this.businessId = businessId;
    }

    public Long getBusinessId() 
    {
        return businessId;
    }
    public void setContactName(String contactName) 
    {
        this.contactName = contactName;
    }

    public String getContactName() 
    {
        return contactName;
    }
    public void setContactPhone(String contactPhone) 
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone() 
    {
        return contactPhone;
    }
    public void setContactEmail(String contactEmail) 
    {
        this.contactEmail = contactEmail;
    }

    public String getContactEmail() 
    {
        return contactEmail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("technologyNeedId", getTechnologyNeedId())
            .append("title", getTitle())
            .append("publishTime", getPublishTime())
            .append("description", getDescription())
            .append("category", getCategory())
            .append("field", getField())
            .append("image", getImage())
            .append("contactId", getContactId())
            .append("businessId", getBusinessId())
            .append("contactName", getContactName())
            .append("contactPhone", getContactPhone())
            .append("contactEmail", getContactEmail())
            .toString();
    }
}
