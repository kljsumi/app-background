package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 人才供应对象 talent_supply
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class TalentSupply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long talentSupplyId;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 照片 */
    @Excel(name = "照片")
    private String image;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;

    /** 专业 */
    @Excel(name = "专业")
    private String profession;

    /** 性别 [1:男，2：女] */
    @Excel(name = "性别 [1:男，2：女]")
    private Long gender;

    /** 求职意向 */
    @Excel(name = "求职意向")
    private String job;

    /** 联系人id */
    @Excel(name = "联系人id")
    private Long contactId;

    /** 专业技能 */
    @Excel(name = "专业技能")
    private String skill;

    /** 个人经历 */
    @Excel(name = "个人经历")
    private String experience;

    /** 入学时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入学时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date enterDate;

    /** 毕业时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "毕业时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date graduateDate;

    /** 学校id */
    @Excel(name = "学校id")
    private Long schoolId;

    /** 上传简历 */
    @Excel(name = "上传简历")
    private String resumeUrl;

    public void setTalentSupplyId(Long talentSupplyId) 
    {
        this.talentSupplyId = talentSupplyId;
    }

    public Long getTalentSupplyId() 
    {
        return talentSupplyId;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setAge(Long age) 
    {
        this.age = age;
    }

    public Long getAge() 
    {
        return age;
    }
    public void setProfession(String profession) 
    {
        this.profession = profession;
    }

    public String getProfession() 
    {
        return profession;
    }
    public void setGender(Long gender) 
    {
        this.gender = gender;
    }

    public Long getGender() 
    {
        return gender;
    }
    public void setJob(String job) 
    {
        this.job = job;
    }

    public String getJob() 
    {
        return job;
    }
    public void setContactId(Long contactId) 
    {
        this.contactId = contactId;
    }

    public Long getContactId() 
    {
        return contactId;
    }
    public void setSkill(String skill) 
    {
        this.skill = skill;
    }

    public String getSkill() 
    {
        return skill;
    }
    public void setExperience(String experience) 
    {
        this.experience = experience;
    }

    public String getExperience() 
    {
        return experience;
    }
    public void setEnterDate(Date enterDate) 
    {
        this.enterDate = enterDate;
    }

    public Date getEnterDate() 
    {
        return enterDate;
    }
    public void setGraduateDate(Date graduateDate) 
    {
        this.graduateDate = graduateDate;
    }

    public Date getGraduateDate() 
    {
        return graduateDate;
    }
    public void setSchoolId(Long schoolId) 
    {
        this.schoolId = schoolId;
    }

    public Long getSchoolId() 
    {
        return schoolId;
    }
    public void setResumeUrl(String resumeUrl) 
    {
        this.resumeUrl = resumeUrl;
    }

    public String getResumeUrl() 
    {
        return resumeUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("talentSupplyId", getTalentSupplyId())
            .append("publishTime", getPublishTime())
            .append("name", getName())
            .append("image", getImage())
            .append("age", getAge())
            .append("profession", getProfession())
            .append("gender", getGender())
            .append("job", getJob())
            .append("contactId", getContactId())
            .append("skill", getSkill())
            .append("experience", getExperience())
            .append("enterDate", getEnterDate())
            .append("graduateDate", getGraduateDate())
            .append("schoolId", getSchoolId())
            .append("resumeUrl", getResumeUrl())
            .toString();
    }
}
