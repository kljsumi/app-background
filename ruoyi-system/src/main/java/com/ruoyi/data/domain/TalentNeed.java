package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 人才需求对象 talent_need
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class TalentNeed extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long talentNeedId;

    /** 职位工作 */
    @Excel(name = "职位工作")
    private String position;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 位置（省市县） */
    @Excel(name = "位置", readConverterExp = "省=市县")
    private String location;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String address;

    /** 联系人id */
    @Excel(name = "联系人id")
    private Long contactId;

    /** 工作内容 */
    @Excel(name = "工作内容")
    private String job;

    /** 职位要求 */
    @Excel(name = "职位要求")
    private String demand;

    /** 详情图片 */
    @Excel(name = "详情图片")
    private String image;

    /** 企业id */
    @Excel(name = "企业id")
    private Long businessId;

    /** 学历 */
    @Excel(name = "学历")
    private String education;

    public void setTalentNeedId(Long talentNeedId) 
    {
        this.talentNeedId = talentNeedId;
    }

    public Long getTalentNeedId() 
    {
        return talentNeedId;
    }
    public void setPosition(String position) 
    {
        this.position = position;
    }

    public String getPosition() 
    {
        return position;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setContactId(Long contactId) 
    {
        this.contactId = contactId;
    }

    public Long getContactId() 
    {
        return contactId;
    }
    public void setJob(String job) 
    {
        this.job = job;
    }

    public String getJob() 
    {
        return job;
    }
    public void setDemand(String demand) 
    {
        this.demand = demand;
    }

    public String getDemand() 
    {
        return demand;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setBusinessId(Long businessId) 
    {
        this.businessId = businessId;
    }

    public Long getBusinessId() 
    {
        return businessId;
    }
    public void setEducation(String education) 
    {
        this.education = education;
    }

    public String getEducation() 
    {
        return education;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("talentNeedId", getTalentNeedId())
            .append("position", getPosition())
            .append("publishTime", getPublishTime())
            .append("location", getLocation())
            .append("address", getAddress())
            .append("contactId", getContactId())
            .append("job", getJob())
            .append("demand", getDemand())
            .append("image", getImage())
            .append("businessId", getBusinessId())
            .append("education", getEducation())
            .toString();
    }
}
