package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 企业信息对象 business_info
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class BusinessInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long businessId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 法人 */
    @Excel(name = "法人")
    private String corporate;

    /** 类型（1：有效期高企，2：培育库入库企业，3：当年申报企业） */
    @Excel(name = "类型", readConverterExp = "1=：有效期高企，2：培育库入库企业，3：当年申报企业")
    private Long type;

    /** 经营范围 */
    @Excel(name = "经营范围")
    private String manegeScope;

    /** 简介 */
    @Excel(name = "简介")
    private String introduction;

    /** 成立日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "成立日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date foundDate;

    /** 经营期限 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "经营期限", width = 30, dateFormat = "yyyy-MM-dd")
    private Date manegeDeadline;

    /** 网址 */
    @Excel(name = "网址")
    private String website;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 注册资本 */
    @Excel(name = "注册资本")
    private String capital;

    /** 位置（省市县） */
    @Excel(name = "位置", readConverterExp = "省=市县")
    private String location;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String address;

    /** 统一社会信用代码 */
    @Excel(name = "统一社会信用代码")
    private String code;

    /** 固定电话 */
    @Excel(name = "固定电话")
    private String telephone;

    /** 联系人id */
    @Excel(name = "联系人id")
    private Long contactId;

    /** 登记机关 */
    @Excel(name = "登记机关")
    private String regOffice;

    /** 核准日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "核准日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date examineDate;

    /** 公司图片 */
    @Excel(name = "公司图片")
    private String imageUrl;

    /** 审核所需资料 */
    @Excel(name = "审核所需资料")
    private String examineImage;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 审核状态 [1：正在审核，2：审核通过，3：审核失败] */
    @Excel(name = "审核状态 [1：正在审核，2：审核通过，3：审核失败]")
    private Long status;

    public void setBusinessId(Long businessId) 
    {
        this.businessId = businessId;
    }

    public Long getBusinessId() 
    {
        return businessId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCorporate(String corporate) 
    {
        this.corporate = corporate;
    }

    public String getCorporate() 
    {
        return corporate;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setManegeScope(String manegeScope) 
    {
        this.manegeScope = manegeScope;
    }

    public String getManegeScope() 
    {
        return manegeScope;
    }
    public void setIntroduction(String introduction) 
    {
        this.introduction = introduction;
    }

    public String getIntroduction() 
    {
        return introduction;
    }
    public void setFoundDate(Date foundDate) 
    {
        this.foundDate = foundDate;
    }

    public Date getFoundDate() 
    {
        return foundDate;
    }
    public void setManegeDeadline(Date manegeDeadline) 
    {
        this.manegeDeadline = manegeDeadline;
    }

    public Date getManegeDeadline() 
    {
        return manegeDeadline;
    }
    public void setWebsite(String website) 
    {
        this.website = website;
    }

    public String getWebsite() 
    {
        return website;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setCapital(String capital) 
    {
        this.capital = capital;
    }

    public String getCapital() 
    {
        return capital;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setTelephone(String telephone) 
    {
        this.telephone = telephone;
    }

    public String getTelephone() 
    {
        return telephone;
    }
    public void setContactId(Long contactId) 
    {
        this.contactId = contactId;
    }

    public Long getContactId() 
    {
        return contactId;
    }
    public void setRegOffice(String regOffice) 
    {
        this.regOffice = regOffice;
    }

    public String getRegOffice() 
    {
        return regOffice;
    }
    public void setExamineDate(Date examineDate) 
    {
        this.examineDate = examineDate;
    }

    public Date getExamineDate() 
    {
        return examineDate;
    }
    public void setImageUrl(String imageUrl) 
    {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() 
    {
        return imageUrl;
    }
    public void setExamineImage(String examineImage) 
    {
        this.examineImage = examineImage;
    }

    public String getExamineImage() 
    {
        return examineImage;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("businessId", getBusinessId())
            .append("name", getName())
            .append("corporate", getCorporate())
            .append("type", getType())
            .append("manegeScope", getManegeScope())
            .append("introduction", getIntroduction())
            .append("foundDate", getFoundDate())
            .append("manegeDeadline", getManegeDeadline())
            .append("website", getWebsite())
            .append("email", getEmail())
            .append("capital", getCapital())
            .append("location", getLocation())
            .append("address", getAddress())
            .append("code", getCode())
            .append("telephone", getTelephone())
            .append("contactId", getContactId())
            .append("regOffice", getRegOffice())
            .append("examineDate", getExamineDate())
            .append("imageUrl", getImageUrl())
            .append("examineImage", getExamineImage())
            .append("userId", getUserId())
            .append("status", getStatus())
            .toString();
    }
}
