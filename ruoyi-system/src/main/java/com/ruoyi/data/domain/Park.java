package com.ruoyi.data.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 科技园区对象 park
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class Park extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 园区id */
    private Long parkId;

    /** 园区名称 */
    @Excel(name = "园区名称")
    private String parkName;

    /** 园区图片url */
    @Excel(name = "园区图片url")
    private String parkImageUrl;

    /** 园区电话 */
    @Excel(name = "园区电话")
    private String parkPhone;

    /** 园区网址 */
    @Excel(name = "园区网址")
    private String parkWebsite;

    /** 园区简介 */
    @Excel(name = "园区简介")
    private String parkIntroduction;

    /** 园区头像 */
    @Excel(name = "园区头像")
    private String avatar;

    /** 配套设施 */
    @Excel(name = "配套设施")
    private String parkSupporting;

    /** 优惠政策 */
    @Excel(name = "优惠政策")
    private String favouredPolicy;

    /** 产品定位 */
    @Excel(name = "产品定位")
    private String productPositioning;

    /** 招商重点 */
    @Excel(name = "招商重点")
    private String keyPoints;

    public void setParkId(Long parkId) 
    {
        this.parkId = parkId;
    }

    public Long getParkId() 
    {
        return parkId;
    }
    public void setParkName(String parkName) 
    {
        this.parkName = parkName;
    }

    public String getParkName() 
    {
        return parkName;
    }
    public void setParkImageUrl(String parkImageUrl) 
    {
        this.parkImageUrl = parkImageUrl;
    }

    public String getParkImageUrl() 
    {
        return parkImageUrl;
    }
    public void setParkPhone(String parkPhone) 
    {
        this.parkPhone = parkPhone;
    }

    public String getParkPhone() 
    {
        return parkPhone;
    }
    public void setParkWebsite(String parkWebsite) 
    {
        this.parkWebsite = parkWebsite;
    }

    public String getParkWebsite() 
    {
        return parkWebsite;
    }
    public void setParkIntroduction(String parkIntroduction) 
    {
        this.parkIntroduction = parkIntroduction;
    }

    public String getParkIntroduction() 
    {
        return parkIntroduction;
    }
    public void setAvatar(String avatar) 
    {
        this.avatar = avatar;
    }

    public String getAvatar() 
    {
        return avatar;
    }
    public void setParkSupporting(String parkSupporting) 
    {
        this.parkSupporting = parkSupporting;
    }

    public String getParkSupporting() 
    {
        return parkSupporting;
    }
    public void setFavouredPolicy(String favouredPolicy) 
    {
        this.favouredPolicy = favouredPolicy;
    }

    public String getFavouredPolicy() 
    {
        return favouredPolicy;
    }
    public void setProductPositioning(String productPositioning) 
    {
        this.productPositioning = productPositioning;
    }

    public String getProductPositioning() 
    {
        return productPositioning;
    }
    public void setKeyPoints(String keyPoints) 
    {
        this.keyPoints = keyPoints;
    }

    public String getKeyPoints() 
    {
        return keyPoints;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("parkId", getParkId())
            .append("parkName", getParkName())
            .append("parkImageUrl", getParkImageUrl())
            .append("parkPhone", getParkPhone())
            .append("parkWebsite", getParkWebsite())
            .append("parkIntroduction", getParkIntroduction())
            .append("avatar", getAvatar())
            .append("parkSupporting", getParkSupporting())
            .append("favouredPolicy", getFavouredPolicy())
            .append("productPositioning", getProductPositioning())
            .append("keyPoints", getKeyPoints())
            .toString();
    }
}
