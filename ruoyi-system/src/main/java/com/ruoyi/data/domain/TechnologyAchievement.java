package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 技术成果对象 technology_achievement
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class TechnologyAchievement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long technologyAchievementId;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 分类 */
    @Excel(name = "分类")
    private String category;

    /** 网址 */
    @Excel(name = "网址")
    private String website;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 行业领域 */
    @Excel(name = "行业领域")
    private String field;

    /** 详情图片 */
    @Excel(name = "详情图片")
    private String image;

    /** 联系人id */
    @Excel(name = "联系人id")
    private Long contactId;

    /** 发布者类型 [1：高新企业，3：学校] */
    @Excel(name = "发布者类型 [1：高新企业，3：学校]")
    private Long publisherType;

    /** 企业或者学校id，根据发布者类型判断 */
    @Excel(name = "企业或者学校id，根据发布者类型判断")
    private Long publisherId;

    public void setTechnologyAchievementId(Long technologyAchievementId) 
    {
        this.technologyAchievementId = technologyAchievementId;
    }

    public Long getTechnologyAchievementId() 
    {
        return technologyAchievementId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setWebsite(String website) 
    {
        this.website = website;
    }

    public String getWebsite() 
    {
        return website;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setField(String field) 
    {
        this.field = field;
    }

    public String getField() 
    {
        return field;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setContactId(Long contactId) 
    {
        this.contactId = contactId;
    }

    public Long getContactId() 
    {
        return contactId;
    }
    public void setPublisherType(Long publisherType) 
    {
        this.publisherType = publisherType;
    }

    public Long getPublisherType() 
    {
        return publisherType;
    }
    public void setPublisherId(Long publisherId) 
    {
        this.publisherId = publisherId;
    }

    public Long getPublisherId() 
    {
        return publisherId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("technologyAchievementId", getTechnologyAchievementId())
            .append("title", getTitle())
            .append("publishTime", getPublishTime())
            .append("category", getCategory())
            .append("website", getWebsite())
            .append("description", getDescription())
            .append("field", getField())
            .append("image", getImage())
            .append("contactId", getContactId())
            .append("publisherType", getPublisherType())
            .append("publisherId", getPublisherId())
            .toString();
    }
}
