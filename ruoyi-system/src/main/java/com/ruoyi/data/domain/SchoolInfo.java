package com.ruoyi.data.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 学校信息对象 school_info
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class SchoolInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long schoolId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 网址 */
    @Excel(name = "网址")
    private String website;

    /** 位置（省市县） */
    @Excel(name = "位置", readConverterExp = "省=市县")
    private String location;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String address;

    /** 统一社会信用代码 */
    @Excel(name = "统一社会信用代码")
    private String code;

    /** 固定电话 */
    @Excel(name = "固定电话")
    private String telephone;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 联系人id */
    @Excel(name = "联系人id")
    private Long contactId;

    /** 学校图片 */
    @Excel(name = "学校图片")
    private String imageUrl;

    /** 审核所需资料 */
    @Excel(name = "审核所需资料")
    private String examineImage;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 审核状态 [1：正在审核，2：审核通过，3：审核失败] */
    @Excel(name = "审核状态 [1：正在审核，2：审核通过，3：审核失败]")
    private Long status;

    public void setSchoolId(Long schoolId) 
    {
        this.schoolId = schoolId;
    }

    public Long getSchoolId() 
    {
        return schoolId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setWebsite(String website) 
    {
        this.website = website;
    }

    public String getWebsite() 
    {
        return website;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setTelephone(String telephone) 
    {
        this.telephone = telephone;
    }

    public String getTelephone() 
    {
        return telephone;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setContactId(Long contactId) 
    {
        this.contactId = contactId;
    }

    public Long getContactId() 
    {
        return contactId;
    }
    public void setImageUrl(String imageUrl) 
    {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() 
    {
        return imageUrl;
    }
    public void setExamineImage(String examineImage) 
    {
        this.examineImage = examineImage;
    }

    public String getExamineImage() 
    {
        return examineImage;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("schoolId", getSchoolId())
            .append("name", getName())
            .append("website", getWebsite())
            .append("location", getLocation())
            .append("address", getAddress())
            .append("code", getCode())
            .append("telephone", getTelephone())
            .append("email", getEmail())
            .append("contactId", getContactId())
            .append("imageUrl", getImageUrl())
            .append("examineImage", getExamineImage())
            .append("userId", getUserId())
            .append("status", getStatus())
            .toString();
    }
}
