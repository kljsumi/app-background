package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 服务产品对象 service_product
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class ServiceProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long serviceProductId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String title;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 产品描述 */
    @Excel(name = "产品描述")
    private String description;

    /** 服务类型 */
    @Excel(name = "服务类型")
    private String serviceType;

    /** 详情图片 */
    @Excel(name = "详情图片")
    private String image;

    /** 联系人id */
    @Excel(name = "联系人id")
    private Long contactId;

    /** 服务机构id */
    @Excel(name = "服务机构id")
    private Long serviceInstitutionId;

    public void setServiceProductId(Long serviceProductId) 
    {
        this.serviceProductId = serviceProductId;
    }

    public Long getServiceProductId() 
    {
        return serviceProductId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setServiceType(String serviceType) 
    {
        this.serviceType = serviceType;
    }

    public String getServiceType() 
    {
        return serviceType;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setContactId(Long contactId) 
    {
        this.contactId = contactId;
    }

    public Long getContactId() 
    {
        return contactId;
    }
    public void setServiceInstitutionId(Long serviceInstitutionId) 
    {
        this.serviceInstitutionId = serviceInstitutionId;
    }

    public Long getServiceInstitutionId() 
    {
        return serviceInstitutionId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("serviceProductId", getServiceProductId())
            .append("title", getTitle())
            .append("publishTime", getPublishTime())
            .append("description", getDescription())
            .append("serviceType", getServiceType())
            .append("image", getImage())
            .append("contactId", getContactId())
            .append("serviceInstitutionId", getServiceInstitutionId())
            .toString();
    }
}
