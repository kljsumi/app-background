package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 咨询对象 consultation
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class Consultation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 咨询内容类型（1：申报政策 2：金融政策 3：科技服务 4：人才政策 5：科技园区 6：其他） */
    @Excel(name = "咨询内容类型", readConverterExp = "1=：申报政策,2=：金融政策,3=：科技服务,4=：人才政策,5=：科技园区,6=：其他")
    private String category;

    /** 咨询内容 */
    @Excel(name = "咨询内容")
    private String content;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phone;

    /** 回复信息对应的咨询信息的id（咨询信息的parent_id为0）
 */
    @Excel(name = "回复信息对应的咨询信息的id", readConverterExp = "咨=询信息的parent_id为0")
    private Long parentId;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 是否已读（0：未读 1：已读） */
    @Excel(name = "是否已读", readConverterExp = "0=：未读,1=：已读")
    private Long isRead;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setIsRead(Long isRead) 
    {
        this.isRead = isRead;
    }

    public Long getIsRead() 
    {
        return isRead;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("category", getCategory())
            .append("content", getContent())
            .append("email", getEmail())
            .append("phone", getPhone())
            .append("parentId", getParentId())
            .append("publishTime", getPublishTime())
            .append("isRead", getIsRead())
            .toString();
    }
}
