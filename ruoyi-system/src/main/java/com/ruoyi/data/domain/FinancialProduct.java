package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 金融产品对象 financial_product
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class FinancialProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long financialProductId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String title;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 产品描述 */
    @Excel(name = "产品描述")
    private String description;

    /** 详情图片 */
    @Excel(name = "详情图片")
    private String image;

    /** 利率范围 （1%-2%） */
    @Excel(name = "利率范围 ", readConverterExp = "1=%-2%")
    private String rate;

    /** 担保额度 */
    @Excel(name = "担保额度")
    private String loanMoney;

    /** 担保方式 */
    @Excel(name = "担保方式")
    private String ensureMethod;

    /** 贷款期限 */
    @Excel(name = "贷款期限")
    private String loanDeadline;

    /** 申请条件 */
    @Excel(name = "申请条件")
    private String applyCondition;

    /** 联系人id */
    @Excel(name = "联系人id")
    private Long contactId;

    /** 金融机构id */
    @Excel(name = "金融机构id")
    private Long financialInstitutionId;

    public void setFinancialProductId(Long financialProductId) 
    {
        this.financialProductId = financialProductId;
    }

    public Long getFinancialProductId() 
    {
        return financialProductId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setRate(String rate) 
    {
        this.rate = rate;
    }

    public String getRate() 
    {
        return rate;
    }
    public void setLoanMoney(String loanMoney) 
    {
        this.loanMoney = loanMoney;
    }

    public String getLoanMoney() 
    {
        return loanMoney;
    }
    public void setEnsureMethod(String ensureMethod) 
    {
        this.ensureMethod = ensureMethod;
    }

    public String getEnsureMethod() 
    {
        return ensureMethod;
    }
    public void setLoanDeadline(String loanDeadline) 
    {
        this.loanDeadline = loanDeadline;
    }

    public String getLoanDeadline() 
    {
        return loanDeadline;
    }
    public void setApplyCondition(String applyCondition) 
    {
        this.applyCondition = applyCondition;
    }

    public String getApplyCondition() 
    {
        return applyCondition;
    }
    public void setContactId(Long contactId) 
    {
        this.contactId = contactId;
    }

    public Long getContactId() 
    {
        return contactId;
    }
    public void setFinancialInstitutionId(Long financialInstitutionId) 
    {
        this.financialInstitutionId = financialInstitutionId;
    }

    public Long getFinancialInstitutionId() 
    {
        return financialInstitutionId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("financialProductId", getFinancialProductId())
            .append("title", getTitle())
            .append("publishTime", getPublishTime())
            .append("description", getDescription())
            .append("image", getImage())
            .append("rate", getRate())
            .append("loanMoney", getLoanMoney())
            .append("ensureMethod", getEnsureMethod())
            .append("loanDeadline", getLoanDeadline())
            .append("applyCondition", getApplyCondition())
            .append("contactId", getContactId())
            .append("financialInstitutionId", getFinancialInstitutionId())
            .toString();
    }
}
