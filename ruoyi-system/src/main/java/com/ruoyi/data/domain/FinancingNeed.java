package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 融资需求对象 financing_need
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class FinancingNeed extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long financingNeedId;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String title;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 项目描述 */
    @Excel(name = "项目描述")
    private String projectDescription;

    /** 行业领域 */
    @Excel(name = "行业领域")
    private String field;

    /** 项目详情图片 */
    @Excel(name = "项目详情图片")
    private String projectImage;

    /** 团队介绍 */
    @Excel(name = "团队介绍")
    private String teamIntroduction;

    /** 融资介绍 */
    @Excel(name = "融资介绍")
    private String financingIntroduction;

    /** 融资金额 */
    @Excel(name = "融资金额")
    private String financingMoney;

    /** 融资方式 */
    @Excel(name = "融资方式")
    private String financingMethod;

    /** 融资阶段 */
    @Excel(name = "融资阶段")
    private String financingPhase;

    /** 联系人id */
    @Excel(name = "联系人id")
    private Long contactId;

    /** 企业id */
    @Excel(name = "企业id")
    private Long businessId;

    public void setFinancingNeedId(Long financingNeedId) 
    {
        this.financingNeedId = financingNeedId;
    }

    public Long getFinancingNeedId() 
    {
        return financingNeedId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setProjectDescription(String projectDescription) 
    {
        this.projectDescription = projectDescription;
    }

    public String getProjectDescription() 
    {
        return projectDescription;
    }
    public void setField(String field) 
    {
        this.field = field;
    }

    public String getField() 
    {
        return field;
    }
    public void setProjectImage(String projectImage) 
    {
        this.projectImage = projectImage;
    }

    public String getProjectImage() 
    {
        return projectImage;
    }
    public void setTeamIntroduction(String teamIntroduction) 
    {
        this.teamIntroduction = teamIntroduction;
    }

    public String getTeamIntroduction() 
    {
        return teamIntroduction;
    }
    public void setFinancingIntroduction(String financingIntroduction) 
    {
        this.financingIntroduction = financingIntroduction;
    }

    public String getFinancingIntroduction() 
    {
        return financingIntroduction;
    }
    public void setFinancingMoney(String financingMoney) 
    {
        this.financingMoney = financingMoney;
    }

    public String getFinancingMoney() 
    {
        return financingMoney;
    }
    public void setFinancingMethod(String financingMethod) 
    {
        this.financingMethod = financingMethod;
    }

    public String getFinancingMethod() 
    {
        return financingMethod;
    }
    public void setFinancingPhase(String financingPhase) 
    {
        this.financingPhase = financingPhase;
    }

    public String getFinancingPhase() 
    {
        return financingPhase;
    }
    public void setContactId(Long contactId) 
    {
        this.contactId = contactId;
    }

    public Long getContactId() 
    {
        return contactId;
    }
    public void setBusinessId(Long businessId) 
    {
        this.businessId = businessId;
    }

    public Long getBusinessId() 
    {
        return businessId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("financingNeedId", getFinancingNeedId())
            .append("title", getTitle())
            .append("publishTime", getPublishTime())
            .append("projectDescription", getProjectDescription())
            .append("field", getField())
            .append("projectImage", getProjectImage())
            .append("teamIntroduction", getTeamIntroduction())
            .append("financingIntroduction", getFinancingIntroduction())
            .append("financingMoney", getFinancingMoney())
            .append("financingMethod", getFinancingMethod())
            .append("financingPhase", getFinancingPhase())
            .append("contactId", getContactId())
            .append("businessId", getBusinessId())
            .toString();
    }
}
