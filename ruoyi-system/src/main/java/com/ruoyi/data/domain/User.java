package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 用户对象 user
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class User extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户id */
    private Long userId;

    /** 昵称 */
    @Excel(name = "昵称")
    private String username;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String realName;

    /** 性别 [1:男，2：女] */
    @Excel(name = "性别 [1:男，2：女]")
    private Long gender;

    /** 位置（省市县） */
    @Excel(name = "位置", readConverterExp = "省=市县")
    private String location;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 电子邮箱 */
    @Excel(name = "电子邮箱")
    private String email;

    /** 用户类型（0:未认定用户，1：高新企业，2：科技主管部门，3：学校，4：金融机构，5：服务机构） */
    @Excel(name = "用户类型", readConverterExp = "0=:未认定用户，1：高新企业，2：科技主管部门，3：学校，4：金融机构，5：服务机构")
    private Long type;

    /** 审核状态（0:未认定，1：正在审核，2：审核通过） */
    @Excel(name = "审核状态", readConverterExp = "0=:未认定，1：正在审核，2：审核通过")
    private Long status;

    /** token */
    @Excel(name = "token")
    private String token;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "token", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "token", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    /** $column.columnComment */
    @Excel(name = "token")
    private Long deleted;

    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setRealName(String realName) 
    {
        this.realName = realName;
    }

    public String getRealName() 
    {
        return realName;
    }
    public void setGender(Long gender) 
    {
        this.gender = gender;
    }

    public Long getGender() 
    {
        return gender;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setToken(String token) 
    {
        this.token = token;
    }

    public String getToken() 
    {
        return token;
    }
    public void setGmtCreate(Date gmtCreate) 
    {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtCreate() 
    {
        return gmtCreate;
    }
    public void setGmtModified(Date gmtModified) 
    {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() 
    {
        return gmtModified;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("realName", getRealName())
            .append("gender", getGender())
            .append("location", getLocation())
            .append("address", getAddress())
            .append("phone", getPhone())
            .append("email", getEmail())
            .append("type", getType())
            .append("status", getStatus())
            .append("token", getToken())
            .append("gmtCreate", getGmtCreate())
            .append("gmtModified", getGmtModified())
            .append("deleted", getDeleted())
            .toString();
    }
}
