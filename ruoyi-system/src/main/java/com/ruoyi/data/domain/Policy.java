package com.ruoyi.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 政策对象 policy
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public class Policy extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long policyId;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 文号 */
    @Excel(name = "文号")
    private String number;

    /** 分类 */
    @Excel(name = "分类")
    private String category;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 链接 */
    @Excel(name = "链接")
    private String link;

    /** 附件 */
    @Excel(name = "附件")
    private String addition;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "用户id", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "用户id", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    /** $column.columnComment */
    @Excel(name = "用户id")
    private Long deleted;

    public void setPolicyId(Long policyId) 
    {
        this.policyId = policyId;
    }

    public Long getPolicyId() 
    {
        return policyId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setLink(String link) 
    {
        this.link = link;
    }

    public String getLink() 
    {
        return link;
    }
    public void setAddition(String addition) 
    {
        this.addition = addition;
    }

    public String getAddition() 
    {
        return addition;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setGmtCreate(Date gmtCreate) 
    {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtCreate() 
    {
        return gmtCreate;
    }
    public void setGmtModified(Date gmtModified) 
    {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() 
    {
        return gmtModified;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("policyId", getPolicyId())
            .append("title", getTitle())
            .append("publishTime", getPublishTime())
            .append("number", getNumber())
            .append("category", getCategory())
            .append("content", getContent())
            .append("link", getLink())
            .append("addition", getAddition())
            .append("userId", getUserId())
            .append("gmtCreate", getGmtCreate())
            .append("gmtModified", getGmtModified())
            .append("deleted", getDeleted())
            .toString();
    }
}
