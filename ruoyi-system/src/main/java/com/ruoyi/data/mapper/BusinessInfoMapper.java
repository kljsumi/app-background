package com.ruoyi.data.mapper;

import com.ruoyi.data.domain.BusinessInfo;

import java.util.List;

/**
 * 企业信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface BusinessInfoMapper 
{
    /**
     * 查询企业信息
     * 
     * @param businessId 企业信息主键
     * @return 企业信息
     */
    public BusinessInfo selectBusinessInfoByBusinessId(Long businessId);

    /**
     * 查询企业信息列表
     * 
     * @param businessInfo 企业信息
     * @return 企业信息集合
     */
    public List<BusinessInfo> selectBusinessInfoList(BusinessInfo businessInfo);

    /**
     * 新增企业信息
     * 
     * @param businessInfo 企业信息
     * @return 结果
     */
    public int insertBusinessInfo(BusinessInfo businessInfo);

    /**
     * 修改企业信息
     * 
     * @param businessInfo 企业信息
     * @return 结果
     */
    public int updateBusinessInfo(BusinessInfo businessInfo);

    /**
     * 删除企业信息
     * 
     * @param businessId 企业信息主键
     * @return 结果
     */
    public int deleteBusinessInfoByBusinessId(Long businessId);

    /**
     * 批量删除企业信息
     * 
     * @param businessIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusinessInfoByBusinessIds(Long[] businessIds);
}
