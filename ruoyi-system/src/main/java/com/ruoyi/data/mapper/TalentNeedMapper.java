package com.ruoyi.data.mapper;

import com.ruoyi.data.domain.TalentNeed;

import java.util.List;

/**
 * 人才需求Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface TalentNeedMapper 
{
    /**
     * 查询人才需求
     * 
     * @param talentNeedId 人才需求主键
     * @return 人才需求
     */
    public TalentNeed selectTalentNeedByTalentNeedId(Long talentNeedId);

    /**
     * 查询人才需求列表
     * 
     * @param talentNeed 人才需求
     * @return 人才需求集合
     */
    public List<TalentNeed> selectTalentNeedList(TalentNeed talentNeed);

    /**
     * 新增人才需求
     * 
     * @param talentNeed 人才需求
     * @return 结果
     */
    public int insertTalentNeed(TalentNeed talentNeed);

    /**
     * 修改人才需求
     * 
     * @param talentNeed 人才需求
     * @return 结果
     */
    public int updateTalentNeed(TalentNeed talentNeed);

    /**
     * 删除人才需求
     * 
     * @param talentNeedId 人才需求主键
     * @return 结果
     */
    public int deleteTalentNeedByTalentNeedId(Long talentNeedId);

    /**
     * 批量删除人才需求
     * 
     * @param talentNeedIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTalentNeedByTalentNeedIds(Long[] talentNeedIds);
}
