package com.ruoyi.data.mapper;

import com.ruoyi.data.domain.TechnologyAchievement;

import java.util.List;

/**
 * 技术成果Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface TechnologyAchievementMapper 
{
    /**
     * 查询技术成果
     * 
     * @param technologyAchievementId 技术成果主键
     * @return 技术成果
     */
    public TechnologyAchievement selectTechnologyAchievementByTechnologyAchievementId(Long technologyAchievementId);

    /**
     * 查询技术成果列表
     * 
     * @param technologyAchievement 技术成果
     * @return 技术成果集合
     */
    public List<TechnologyAchievement> selectTechnologyAchievementList(TechnologyAchievement technologyAchievement);

    /**
     * 新增技术成果
     * 
     * @param technologyAchievement 技术成果
     * @return 结果
     */
    public int insertTechnologyAchievement(TechnologyAchievement technologyAchievement);

    /**
     * 修改技术成果
     * 
     * @param technologyAchievement 技术成果
     * @return 结果
     */
    public int updateTechnologyAchievement(TechnologyAchievement technologyAchievement);

    /**
     * 删除技术成果
     * 
     * @param technologyAchievementId 技术成果主键
     * @return 结果
     */
    public int deleteTechnologyAchievementByTechnologyAchievementId(Long technologyAchievementId);

    /**
     * 批量删除技术成果
     * 
     * @param technologyAchievementIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTechnologyAchievementByTechnologyAchievementIds(Long[] technologyAchievementIds);
}
