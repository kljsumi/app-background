package com.ruoyi.data.mapper;

import com.ruoyi.data.domain.ServiceProduct;

import java.util.List;

/**
 * 服务产品Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface ServiceProductMapper 
{
    /**
     * 查询服务产品
     * 
     * @param serviceProductId 服务产品主键
     * @return 服务产品
     */
    public ServiceProduct selectServiceProductByServiceProductId(Long serviceProductId);

    /**
     * 查询服务产品列表
     * 
     * @param serviceProduct 服务产品
     * @return 服务产品集合
     */
    public List<ServiceProduct> selectServiceProductList(ServiceProduct serviceProduct);

    /**
     * 新增服务产品
     * 
     * @param serviceProduct 服务产品
     * @return 结果
     */
    public int insertServiceProduct(ServiceProduct serviceProduct);

    /**
     * 修改服务产品
     * 
     * @param serviceProduct 服务产品
     * @return 结果
     */
    public int updateServiceProduct(ServiceProduct serviceProduct);

    /**
     * 删除服务产品
     * 
     * @param serviceProductId 服务产品主键
     * @return 结果
     */
    public int deleteServiceProductByServiceProductId(Long serviceProductId);

    /**
     * 批量删除服务产品
     * 
     * @param serviceProductIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteServiceProductByServiceProductIds(Long[] serviceProductIds);
}
