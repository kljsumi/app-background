package com.ruoyi.data.mapper;

import com.ruoyi.data.domain.AllIndex;

import java.util.List;

/**
 * 索引Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface AllIndexMapper 
{
    /**
     * 查询索引
     * 
     * @param id 索引主键
     * @return 索引
     */
    public AllIndex selectAllIndexById(Long id);

    /**
     * 查询索引列表
     * 
     * @param allIndex 索引
     * @return 索引集合
     */
    public List<AllIndex> selectAllIndexList(AllIndex allIndex);

    /**
     * 新增索引
     * 
     * @param allIndex 索引
     * @return 结果
     */
    public int insertAllIndex(AllIndex allIndex);

    /**
     * 修改索引
     * 
     * @param allIndex 索引
     * @return 结果
     */
    public int updateAllIndex(AllIndex allIndex);

    /**
     * 删除索引
     * 
     * @param id 索引主键
     * @return 结果
     */
    public int deleteAllIndexById(Long id);

    /**
     * 批量删除索引
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAllIndexByIds(Long[] ids);

    List<AllIndex> getIndexByName(String listName);
}
