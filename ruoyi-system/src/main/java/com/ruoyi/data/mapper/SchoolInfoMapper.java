package com.ruoyi.data.mapper;

import com.ruoyi.data.domain.SchoolInfo;

import java.util.List;

/**
 * 学校信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
public interface SchoolInfoMapper 
{
    /**
     * 查询学校信息
     * 
     * @param schoolId 学校信息主键
     * @return 学校信息
     */
    public SchoolInfo selectSchoolInfoBySchoolId(Long schoolId);

    /**
     * 查询学校信息列表
     * 
     * @param schoolInfo 学校信息
     * @return 学校信息集合
     */
    public List<SchoolInfo> selectSchoolInfoList(SchoolInfo schoolInfo);

    /**
     * 新增学校信息
     * 
     * @param schoolInfo 学校信息
     * @return 结果
     */
    public int insertSchoolInfo(SchoolInfo schoolInfo);

    /**
     * 修改学校信息
     * 
     * @param schoolInfo 学校信息
     * @return 结果
     */
    public int updateSchoolInfo(SchoolInfo schoolInfo);

    /**
     * 删除学校信息
     * 
     * @param schoolId 学校信息主键
     * @return 结果
     */
    public int deleteSchoolInfoBySchoolId(Long schoolId);

    /**
     * 批量删除学校信息
     * 
     * @param schoolIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSchoolInfoBySchoolIds(Long[] schoolIds);
}
