import request from '@/utils/request'

// 查询金融产品列表
export function listFinancialProduct(query) {
  return request({
    url: '/data/financialProduct/list',
    method: 'get',
    params: query
  })
}

// 查询金融产品详细
export function getFinancialProduct(financialProductId) {
  return request({
    url: '/data/financialProduct/' + financialProductId,
    method: 'get'
  })
}

// 新增金融产品
export function addFinancialProduct(data) {
  return request({
    url: '/data/financialProduct',
    method: 'post',
    data: data
  })
}

// 修改金融产品
export function updateFinancialProduct(data) {
  return request({
    url: '/data/financialProduct',
    method: 'put',
    data: data
  })
}

// 删除金融产品
export function delFinancialProduct(financialProductId) {
  return request({
    url: '/data/financialProduct/' + financialProductId,
    method: 'delete'
  })
}

// 导出金融产品
export function exportFinancialProduct(query) {
  return request({
    url: '/data/financialProduct/export',
    method: 'get',
    params: query
  })
}