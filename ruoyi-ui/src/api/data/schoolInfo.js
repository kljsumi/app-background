import request from '@/utils/request'

// 查询学校信息列表
export function listSchoolInfo(query) {
  return request({
    url: '/data/schoolInfo/list',
    method: 'get',
    params: query
  })
}

// 查询学校信息详细
export function getSchoolInfo(schoolId) {
  return request({
    url: '/data/schoolInfo/' + schoolId,
    method: 'get'
  })
}

// 新增学校信息
export function addSchoolInfo(data) {
  return request({
    url: '/data/schoolInfo',
    method: 'post',
    data: data
  })
}

// 修改学校信息
export function updateSchoolInfo(data) {
  return request({
    url: '/data/schoolInfo',
    method: 'put',
    data: data
  })
}

// 删除学校信息
export function delSchoolInfo(schoolId) {
  return request({
    url: '/data/schoolInfo/' + schoolId,
    method: 'delete'
  })
}

// 导出学校信息
export function exportSchoolInfo(query) {
  return request({
    url: '/data/schoolInfo/export',
    method: 'get',
    params: query
  })
}