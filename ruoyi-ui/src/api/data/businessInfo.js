import request from '@/utils/request'

// 查询企业信息列表
export function listBusinessInfo(query) {
  return request({
    url: '/data/businessInfo/list',
    method: 'get',
    params: query
  })
}

// 查询企业信息详细
export function getBusinessInfo(businessId) {
  return request({
    url: '/data/businessInfo/' + businessId,
    method: 'get'
  })
}

// 新增企业信息
export function addBusinessInfo(data) {
  return request({
    url: '/data/businessInfo',
    method: 'post',
    data: data
  })
}

// 修改企业信息
export function updateBusinessInfo(data) {
  return request({
    url: '/data/businessInfo',
    method: 'put',
    data: data
  })
}

// 删除企业信息
export function delBusinessInfo(businessId) {
  return request({
    url: '/data/businessInfo/' + businessId,
    method: 'delete'
  })
}

// 导出企业信息
export function exportBusinessInfo(query) {
  return request({
    url: '/data/businessInfo/export',
    method: 'get',
    params: query
  })
}