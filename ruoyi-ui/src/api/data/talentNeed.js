import request from '@/utils/request'

// 查询人才需求列表
export function listTalentNeed(query) {
  return request({
    url: '/data/talentNeed/list',
    method: 'get',
    params: query
  })
}

// 查询人才需求详细
export function getTalentNeed(talentNeedId) {
  return request({
    url: '/data/talentNeed/' + talentNeedId,
    method: 'get'
  })
}

// 新增人才需求
export function addTalentNeed(data) {
  return request({
    url: '/data/talentNeed',
    method: 'post',
    data: data
  })
}

// 修改人才需求
export function updateTalentNeed(data) {
  return request({
    url: '/data/talentNeed',
    method: 'put',
    data: data
  })
}

// 删除人才需求
export function delTalentNeed(talentNeedId) {
  return request({
    url: '/data/talentNeed/' + talentNeedId,
    method: 'delete'
  })
}

// 导出人才需求
export function exportTalentNeed(query) {
  return request({
    url: '/data/talentNeed/export',
    method: 'get',
    params: query
  })
}