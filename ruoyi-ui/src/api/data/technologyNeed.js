import request from '@/utils/request'

// 查询技术需求列表
export function listTechnologyNeed(query) {
  return request({
    url: '/data/technologyNeed/list',
    method: 'get',
    params: query
  })
}

// 查询技术需求详细
export function getTechnologyNeed(technologyNeedId) {
  return request({
    url: '/data/technologyNeed/' + technologyNeedId,
    method: 'get'
  })
}

// 新增技术需求
export function addTechnologyNeed(data) {
  return request({
    url: '/data/technologyNeed',
    method: 'post',
    data: data
  })
}

// 修改技术需求
export function updateTechnologyNeed(data) {
  return request({
    url: '/data/technologyNeed',
    method: 'put',
    data: data
  })
}

// 删除技术需求
export function delTechnologyNeed(technologyNeedId) {
  return request({
    url: '/data/technologyNeed/' + technologyNeedId,
    method: 'delete'
  })
}

// 导出技术需求
export function exportTechnologyNeed(query) {
  return request({
    url: '/data/technologyNeed/export',
    method: 'get',
    params: query
  })
}