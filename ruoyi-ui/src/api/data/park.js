import request from '@/utils/request'

// 查询科技园区列表
export function listPark(query) {
  return request({
    url: '/data/park/list',
    method: 'get',
    params: query
  })
}

// 查询科技园区详细
export function getPark(parkId) {
  return request({
    url: '/data/park/' + parkId,
    method: 'get'
  })
}

// 新增科技园区
export function addPark(data) {
  return request({
    url: '/data/park',
    method: 'post',
    data: data
  })
}

// 修改科技园区
export function updatePark(data) {
  return request({
    url: '/data/park',
    method: 'put',
    data: data
  })
}

// 删除科技园区
export function delPark(parkId) {
  return request({
    url: '/data/park/' + parkId,
    method: 'delete'
  })
}

// 导出科技园区
export function exportPark(query) {
  return request({
    url: '/data/park/export',
    method: 'get',
    params: query
  })
}