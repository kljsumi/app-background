import request from '@/utils/request'

// 查询技术成果列表
export function listTechnologyAchievement(query) {
  return request({
    url: '/data/technologyAchievement/list',
    method: 'get',
    params: query
  })
}

// 查询技术成果详细
export function getTechnologyAchievement(technologyAchievementId) {
  return request({
    url: '/data/technologyAchievement/' + technologyAchievementId,
    method: 'get'
  })
}

// 新增技术成果
export function addTechnologyAchievement(data) {
  return request({
    url: '/data/technologyAchievement',
    method: 'post',
    data: data
  })
}

// 修改技术成果
export function updateTechnologyAchievement(data) {
  return request({
    url: '/data/technologyAchievement',
    method: 'put',
    data: data
  })
}

// 删除技术成果
export function delTechnologyAchievement(technologyAchievementId) {
  return request({
    url: '/data/technologyAchievement/' + technologyAchievementId,
    method: 'delete'
  })
}

// 导出技术成果
export function exportTechnologyAchievement(query) {
  return request({
    url: '/data/technologyAchievement/export',
    method: 'get',
    params: query
  })
}