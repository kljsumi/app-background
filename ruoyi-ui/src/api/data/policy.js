import request from '@/utils/request'
import { praseStrEmpty } from "@/utils/ruoyi";

// 查询政策列表
export function listPolicy(query) {
  return request({
    url: '/data/policy/list',
    method: 'get',
    params: query
  })
}

// 查询政策详细
export function getPolicy(policyId) {
  return request({
    url: '/data/policy/' + praseStrEmpty(policyId),
    method: 'get'
  })
}

// 新增政策
export function addPolicy(data) {
  return request({
    url: '/data/policy',
    method: 'post',
    data: data
  })
}

// 修改政策
export function updatePolicy(data) {
  return request({
    url: '/data/policy',
    method: 'put',
    data: data
  })
}

// 删除政策
export function delPolicy(policyId) {
  return request({
    url: '/data/policy/' + policyId,
    method: 'delete'
  })
}

// 导出政策
export function exportPolicy(query) {
  return request({
    url: '/data/policy/export',
    method: 'get',
    params: query
  })
}
