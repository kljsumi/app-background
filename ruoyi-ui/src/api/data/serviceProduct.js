import request from '@/utils/request'

// 查询服务产品列表
export function listServiceProduct(query) {
  return request({
    url: '/data/serviceProduct/list',
    method: 'get',
    params: query
  })
}

// 查询服务产品详细
export function getServiceProduct(serviceProductId) {
  return request({
    url: '/data/serviceProduct/' + serviceProductId,
    method: 'get'
  })
}

// 新增服务产品
export function addServiceProduct(data) {
  return request({
    url: '/data/serviceProduct',
    method: 'post',
    data: data
  })
}

// 修改服务产品
export function updateServiceProduct(data) {
  return request({
    url: '/data/serviceProduct',
    method: 'put',
    data: data
  })
}

// 删除服务产品
export function delServiceProduct(serviceProductId) {
  return request({
    url: '/data/serviceProduct/' + serviceProductId,
    method: 'delete'
  })
}

// 导出服务产品
export function exportServiceProduct(query) {
  return request({
    url: '/data/serviceProduct/export',
    method: 'get',
    params: query
  })
}