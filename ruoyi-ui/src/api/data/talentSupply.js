import request from '@/utils/request'

// 查询人才供应列表
export function listTalentSupply(query) {
  return request({
    url: '/data/talentSupply/list',
    method: 'get',
    params: query
  })
}

// 查询人才供应详细
export function getTalentSupply(talentSupplyId) {
  return request({
    url: '/data/talentSupply/' + talentSupplyId,
    method: 'get'
  })
}

// 新增人才供应
export function addTalentSupply(data) {
  return request({
    url: '/data/talentSupply',
    method: 'post',
    data: data
  })
}

// 修改人才供应
export function updateTalentSupply(data) {
  return request({
    url: '/data/talentSupply',
    method: 'put',
    data: data
  })
}

// 删除人才供应
export function delTalentSupply(talentSupplyId) {
  return request({
    url: '/data/talentSupply/' + talentSupplyId,
    method: 'delete'
  })
}

// 导出人才供应
export function exportTalentSupply(query) {
  return request({
    url: '/data/talentSupply/export',
    method: 'get',
    params: query
  })
}