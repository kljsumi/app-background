import request from '@/utils/request'

// 查询索引列表
export function listAllIndex(query) {
  return request({
    url: '/data/allIndex/list',
    method: 'get',
    params: query
  })
}

// 查询索引详细
export function getAllIndex(id) {
  return request({
    url: '/data/allIndex/' + id,
    method: 'get'
  })
}

// 新增索引
export function addAllIndex(data) {
  return request({
    url: '/data/allIndex',
    method: 'post',
    data: data
  })
}

// 修改索引
export function updateAllIndex(data) {
  return request({
    url: '/data/allIndex',
    method: 'put',
    data: data
  })
}

// 删除索引
export function delAllIndex(id) {
  return request({
    url: '/data/allIndex/' + id,
    method: 'delete'
  })
}

// 导出索引
export function exportAllIndex(query) {
  return request({
    url: '/data/allIndex/export',
    method: 'get',
    params: query
  })
}