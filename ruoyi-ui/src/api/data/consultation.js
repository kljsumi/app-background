import request from '@/utils/request'

// 查询咨询列表
export function listConsultation(query) {
  return request({
    url: '/data/consultation/list',
    method: 'get',
    params: query
  })
}

// 查询咨询详细
export function getConsultation(id) {
  return request({
    url: '/data/consultation/' + id,
    method: 'get'
  })
}

// 新增咨询
export function addConsultation(data) {
  return request({
    url: '/data/consultation',
    method: 'post',
    data: data
  })
}

// 修改咨询
export function updateConsultation(data) {
  return request({
    url: '/data/consultation',
    method: 'put',
    data: data
  })
}

// 删除咨询
export function delConsultation(id) {
  return request({
    url: '/data/consultation/' + id,
    method: 'delete'
  })
}

// 导出咨询
export function exportConsultation(query) {
  return request({
    url: '/data/consultation/export',
    method: 'get',
    params: query
  })
}