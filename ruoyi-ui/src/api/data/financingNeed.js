import request from '@/utils/request'

// 查询融资需求列表
export function listFinancingNeed(query) {
  return request({
    url: '/data/financingNeed/list',
    method: 'get',
    params: query
  })
}

// 查询融资需求详细
export function getFinancingNeed(financingNeedId) {
  return request({
    url: '/data/financingNeed/' + financingNeedId,
    method: 'get'
  })
}

// 新增融资需求
export function addFinancingNeed(data) {
  return request({
    url: '/data/financingNeed',
    method: 'post',
    data: data
  })
}

// 修改融资需求
export function updateFinancingNeed(data) {
  return request({
    url: '/data/financingNeed',
    method: 'put',
    data: data
  })
}

// 删除融资需求
export function delFinancingNeed(financingNeedId) {
  return request({
    url: '/data/financingNeed/' + financingNeedId,
    method: 'delete'
  })
}

// 导出融资需求
export function exportFinancingNeed(query) {
  return request({
    url: '/data/financingNeed/export',
    method: 'get',
    params: query
  })
}