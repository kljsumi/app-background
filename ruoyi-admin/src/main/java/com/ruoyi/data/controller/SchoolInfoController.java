package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.SchoolInfo;
import com.ruoyi.data.service.ISchoolInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学校信息Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/schoolInfo")
public class SchoolInfoController extends BaseController
{
    @Autowired
    private ISchoolInfoService schoolInfoService;

    /**
     * 查询学校信息列表
     */
    @PreAuthorize("@ss.hasPermi('data:schoolInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(SchoolInfo schoolInfo)
    {
        startPage();
        List<SchoolInfo> list = schoolInfoService.selectSchoolInfoList(schoolInfo);
        return getDataTable(list);
    }

    /**
     * 导出学校信息列表
     */
    @PreAuthorize("@ss.hasPermi('data:schoolInfo:export')")
    @Log(title = "学校信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SchoolInfo schoolInfo)
    {
        List<SchoolInfo> list = schoolInfoService.selectSchoolInfoList(schoolInfo);
        ExcelUtil<SchoolInfo> util = new ExcelUtil<SchoolInfo>(SchoolInfo.class);
        return util.exportExcel(list, "学校信息数据");
    }

    /**
     * 获取学校信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:schoolInfo:query')")
    @GetMapping(value = "/{schoolId}")
    public AjaxResult getInfo(@PathVariable("schoolId") Long schoolId)
    {
        return AjaxResult.success(schoolInfoService.selectSchoolInfoBySchoolId(schoolId));
    }

    /**
     * 新增学校信息
     */
    @PreAuthorize("@ss.hasPermi('data:schoolInfo:add')")
    @Log(title = "学校信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SchoolInfo schoolInfo)
    {
        return toAjax(schoolInfoService.insertSchoolInfo(schoolInfo));
    }

    /**
     * 修改学校信息
     */
    @PreAuthorize("@ss.hasPermi('data:schoolInfo:edit')")
    @Log(title = "学校信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SchoolInfo schoolInfo)
    {
        return toAjax(schoolInfoService.updateSchoolInfo(schoolInfo));
    }

    /**
     * 删除学校信息
     */
    @PreAuthorize("@ss.hasPermi('data:schoolInfo:remove')")
    @Log(title = "学校信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{schoolIds}")
    public AjaxResult remove(@PathVariable Long[] schoolIds)
    {
        return toAjax(schoolInfoService.deleteSchoolInfoBySchoolIds(schoolIds));
    }
}
