package com.ruoyi.data.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author kLjSumi
 * @Date 2021/11/2 10:49
 */
@RestController
public class OssController {

    @Autowired
    private OssUtil ossUtil;

    /**
     * 单文件上传
     * @param file
     * @return
     */
    @PostMapping("upload/single")
    public AjaxResult upload(@RequestParam("file") MultipartFile file) {
        String url = null;
        try {
            url = ossUtil.upload(file);
        } catch (IOException e) {
            return AjaxResult.error(e.getMessage());
        }
        AjaxResult ajax = AjaxResult.success();
        ajax.put("name", StringUtils.getFileNameFromUrl(url));
        ajax.put("url", url);
        return ajax;
    }

//    /**
//     * 多文件上传
//     * @param files
//     * @return
//     */
//    @ApiOperation("多文件上传")
//    @PostMapping("upload/multi")
//    public R upload(@RequestParam("files") MultipartFile[] files) {
//        List<String> urls = null;
//        try {
//            urls = ossUtil.upload(files);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Map<String,Object> map = new HashMap<>();
//        map.put("urls",urls);
//        return R.ok().put("data",map);
//    }

}
