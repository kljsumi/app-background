package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.AllIndex;
import com.ruoyi.data.service.IAllIndexService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 索引Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/allIndex")
public class AllIndexController extends BaseController
{
    @Autowired
    private IAllIndexService allIndexService;

    /**
     * 查询索引列表
     */
    @PreAuthorize("@ss.hasPermi('data:allIndex:list')")
    @GetMapping("/list")
    public TableDataInfo list(AllIndex allIndex)
    {
        startPage();
        List<AllIndex> list = allIndexService.selectAllIndexList(allIndex);
        return getDataTable(list);
    }

    /**
     * 导出索引列表
     */
    @PreAuthorize("@ss.hasPermi('data:allIndex:export')")
    @Log(title = "索引", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AllIndex allIndex)
    {
        List<AllIndex> list = allIndexService.selectAllIndexList(allIndex);
        ExcelUtil<AllIndex> util = new ExcelUtil<AllIndex>(AllIndex.class);
        return util.exportExcel(list, "索引数据");
    }

    /**
     * 获取索引详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:allIndex:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(allIndexService.selectAllIndexById(id));
    }

    /**
     * 新增索引
     */
    @PreAuthorize("@ss.hasPermi('data:allIndex:add')")
    @Log(title = "索引", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AllIndex allIndex)
    {
        return toAjax(allIndexService.insertAllIndex(allIndex));
    }

    /**
     * 修改索引
     */
    @PreAuthorize("@ss.hasPermi('data:allIndex:edit')")
    @Log(title = "索引", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AllIndex allIndex)
    {
        return toAjax(allIndexService.updateAllIndex(allIndex));
    }

    /**
     * 删除索引
     */
    @PreAuthorize("@ss.hasPermi('data:allIndex:remove')")
    @Log(title = "索引", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(allIndexService.deleteAllIndexByIds(ids));
    }
}
