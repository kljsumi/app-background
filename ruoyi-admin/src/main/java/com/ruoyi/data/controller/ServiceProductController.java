package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.ServiceProduct;
import com.ruoyi.data.service.IServiceProductService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 服务产品Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/serviceProduct")
public class ServiceProductController extends BaseController
{
    @Autowired
    private IServiceProductService serviceProductService;

    /**
     * 查询服务产品列表
     */
    @PreAuthorize("@ss.hasPermi('data:serviceProduct:list')")
    @GetMapping("/list")
    public TableDataInfo list(ServiceProduct serviceProduct)
    {
        startPage();
        List<ServiceProduct> list = serviceProductService.selectServiceProductList(serviceProduct);
        return getDataTable(list);
    }

    /**
     * 导出服务产品列表
     */
    @PreAuthorize("@ss.hasPermi('data:serviceProduct:export')")
    @Log(title = "服务产品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ServiceProduct serviceProduct)
    {
        List<ServiceProduct> list = serviceProductService.selectServiceProductList(serviceProduct);
        ExcelUtil<ServiceProduct> util = new ExcelUtil<ServiceProduct>(ServiceProduct.class);
        return util.exportExcel(list, "服务产品数据");
    }

    /**
     * 获取服务产品详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:serviceProduct:query')")
    @GetMapping(value = "/{serviceProductId}")
    public AjaxResult getInfo(@PathVariable("serviceProductId") Long serviceProductId)
    {
        return AjaxResult.success(serviceProductService.selectServiceProductByServiceProductId(serviceProductId));
    }

    /**
     * 新增服务产品
     */
    @PreAuthorize("@ss.hasPermi('data:serviceProduct:add')")
    @Log(title = "服务产品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ServiceProduct serviceProduct)
    {
        return toAjax(serviceProductService.insertServiceProduct(serviceProduct));
    }

    /**
     * 修改服务产品
     */
    @PreAuthorize("@ss.hasPermi('data:serviceProduct:edit')")
    @Log(title = "服务产品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ServiceProduct serviceProduct)
    {
        return toAjax(serviceProductService.updateServiceProduct(serviceProduct));
    }

    /**
     * 删除服务产品
     */
    @PreAuthorize("@ss.hasPermi('data:serviceProduct:remove')")
    @Log(title = "服务产品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{serviceProductIds}")
    public AjaxResult remove(@PathVariable Long[] serviceProductIds)
    {
        return toAjax(serviceProductService.deleteServiceProductByServiceProductIds(serviceProductIds));
    }
}
