package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.TalentNeed;
import com.ruoyi.data.service.ITalentNeedService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 人才需求Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/talentNeed")
public class TalentNeedController extends BaseController
{
    @Autowired
    private ITalentNeedService talentNeedService;

    /**
     * 查询人才需求列表
     */
    @PreAuthorize("@ss.hasPermi('data:talentNeed:list')")
    @GetMapping("/list")
    public TableDataInfo list(TalentNeed talentNeed)
    {
        startPage();
        List<TalentNeed> list = talentNeedService.selectTalentNeedList(talentNeed);
        return getDataTable(list);
    }

    /**
     * 导出人才需求列表
     */
    @PreAuthorize("@ss.hasPermi('data:talentNeed:export')")
    @Log(title = "人才需求", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TalentNeed talentNeed)
    {
        List<TalentNeed> list = talentNeedService.selectTalentNeedList(talentNeed);
        ExcelUtil<TalentNeed> util = new ExcelUtil<TalentNeed>(TalentNeed.class);
        return util.exportExcel(list, "人才需求数据");
    }

    /**
     * 获取人才需求详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:talentNeed:query')")
    @GetMapping(value = "/{talentNeedId}")
    public AjaxResult getInfo(@PathVariable("talentNeedId") Long talentNeedId)
    {
        return AjaxResult.success(talentNeedService.selectTalentNeedByTalentNeedId(talentNeedId));
    }

    /**
     * 新增人才需求
     */
    @PreAuthorize("@ss.hasPermi('data:talentNeed:add')")
    @Log(title = "人才需求", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TalentNeed talentNeed)
    {
        return toAjax(talentNeedService.insertTalentNeed(talentNeed));
    }

    /**
     * 修改人才需求
     */
    @PreAuthorize("@ss.hasPermi('data:talentNeed:edit')")
    @Log(title = "人才需求", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TalentNeed talentNeed)
    {
        return toAjax(talentNeedService.updateTalentNeed(talentNeed));
    }

    /**
     * 删除人才需求
     */
    @PreAuthorize("@ss.hasPermi('data:talentNeed:remove')")
    @Log(title = "人才需求", businessType = BusinessType.DELETE)
	@DeleteMapping("/{talentNeedIds}")
    public AjaxResult remove(@PathVariable Long[] talentNeedIds)
    {
        return toAjax(talentNeedService.deleteTalentNeedByTalentNeedIds(talentNeedIds));
    }
}
