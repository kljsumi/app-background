package com.ruoyi.data.controller;

import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.BusinessInfo;
import com.ruoyi.data.service.IBusinessInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 企业信息Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/businessInfo")
public class BusinessInfoController extends BaseController
{
    @Autowired
    private IBusinessInfoService businessInfoService;

    /**
     * 查询企业信息列表
     */
    @ApiOperation("1123")
    @PreAuthorize("@ss.hasPermi('data:businessInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusinessInfo businessInfo)
    {
        startPage();
        List<BusinessInfo> list = businessInfoService.selectBusinessInfoList(businessInfo);
        return getDataTable(list);
    }

    /**
     * 导出企业信息列表
     */
    @PreAuthorize("@ss.hasPermi('data:businessInfo:export')")
    @Log(title = "企业信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusinessInfo businessInfo)
    {
        List<BusinessInfo> list = businessInfoService.selectBusinessInfoList(businessInfo);
        ExcelUtil<BusinessInfo> util = new ExcelUtil<BusinessInfo>(BusinessInfo.class);
        return util.exportExcel(list, "企业信息数据");
    }

    /**
     * 获取企业信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:businessInfo:query')")
    @GetMapping(value = "/{businessId}")
    public AjaxResult getInfo(@PathVariable("businessId") Long businessId)
    {
        return AjaxResult.success(businessInfoService.selectBusinessInfoByBusinessId(businessId));
    }

    /**
     * 新增企业信息
     */
    @PreAuthorize("@ss.hasPermi('data:businessInfo:add')")
    @Log(title = "企业信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusinessInfo businessInfo)
    {
        return toAjax(businessInfoService.insertBusinessInfo(businessInfo));
    }

    /**
     * 修改企业信息
     */
    @PreAuthorize("@ss.hasPermi('data:businessInfo:edit')")
    @Log(title = "企业信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusinessInfo businessInfo)
    {
        return toAjax(businessInfoService.updateBusinessInfo(businessInfo));
    }

    /**
     * 删除企业信息
     */
    @PreAuthorize("@ss.hasPermi('data:businessInfo:remove')")
    @Log(title = "企业信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{businessIds}")
    public AjaxResult remove(@PathVariable Long[] businessIds)
    {
        return toAjax(businessInfoService.deleteBusinessInfoByBusinessIds(businessIds));
    }
}
