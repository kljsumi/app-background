package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.TechnologyNeed;
import com.ruoyi.data.service.ITechnologyNeedService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 技术需求Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/technologyNeed")
public class TechnologyNeedController extends BaseController
{
    @Autowired
    private ITechnologyNeedService technologyNeedService;

    /**
     * 查询技术需求列表
     */
    @PreAuthorize("@ss.hasPermi('data:technologyNeed:list')")
    @GetMapping("/list")
    public TableDataInfo list(TechnologyNeed technologyNeed)
    {
        startPage();
        List<TechnologyNeed> list = technologyNeedService.selectTechnologyNeedList(technologyNeed);
        return getDataTable(list);
    }

    /**
     * 导出技术需求列表
     */
    @PreAuthorize("@ss.hasPermi('data:technologyNeed:export')")
    @Log(title = "技术需求", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TechnologyNeed technologyNeed)
    {
        List<TechnologyNeed> list = technologyNeedService.selectTechnologyNeedList(technologyNeed);
        ExcelUtil<TechnologyNeed> util = new ExcelUtil<TechnologyNeed>(TechnologyNeed.class);
        return util.exportExcel(list, "技术需求数据");
    }

    /**
     * 获取技术需求详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:technologyNeed:query')")
    @GetMapping(value = "/{technologyNeedId}")
    public AjaxResult getInfo(@PathVariable("technologyNeedId") Long technologyNeedId)
    {
        return AjaxResult.success(technologyNeedService.selectTechnologyNeedByTechnologyNeedId(technologyNeedId));
    }

    /**
     * 新增技术需求
     */
    @PreAuthorize("@ss.hasPermi('data:technologyNeed:add')")
    @Log(title = "技术需求", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TechnologyNeed technologyNeed)
    {
        return toAjax(technologyNeedService.insertTechnologyNeed(technologyNeed));
    }

    /**
     * 修改技术需求
     */
    @PreAuthorize("@ss.hasPermi('data:technologyNeed:edit')")
    @Log(title = "技术需求", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TechnologyNeed technologyNeed)
    {
        return toAjax(technologyNeedService.updateTechnologyNeed(technologyNeed));
    }

    /**
     * 删除技术需求
     */
    @PreAuthorize("@ss.hasPermi('data:technologyNeed:remove')")
    @Log(title = "技术需求", businessType = BusinessType.DELETE)
	@DeleteMapping("/{technologyNeedIds}")
    public AjaxResult remove(@PathVariable Long[] technologyNeedIds)
    {
        return toAjax(technologyNeedService.deleteTechnologyNeedByTechnologyNeedIds(technologyNeedIds));
    }
}
