package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.Consultation;
import com.ruoyi.data.service.IConsultationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 咨询Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/consultation")
public class ConsultationController extends BaseController
{
    @Autowired
    private IConsultationService consultationService;

    /**
     * 查询咨询列表
     */
    @PreAuthorize("@ss.hasPermi('data:consultation:list')")
    @GetMapping("/list")
    public TableDataInfo list(Consultation consultation)
    {
        startPage();
        List<Consultation> list = consultationService.selectConsultationList(consultation);
        return getDataTable(list);
    }

    /**
     * 导出咨询列表
     */
    @PreAuthorize("@ss.hasPermi('data:consultation:export')")
    @Log(title = "咨询", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Consultation consultation)
    {
        List<Consultation> list = consultationService.selectConsultationList(consultation);
        ExcelUtil<Consultation> util = new ExcelUtil<Consultation>(Consultation.class);
        return util.exportExcel(list, "咨询数据");
    }

    /**
     * 获取咨询详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:consultation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(consultationService.selectConsultationById(id));
    }

    /**
     * 新增咨询
     */
    @PreAuthorize("@ss.hasPermi('data:consultation:add')")
    @Log(title = "咨询", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Consultation consultation)
    {
        return toAjax(consultationService.insertConsultation(consultation));
    }

    /**
     * 修改咨询
     */
    @PreAuthorize("@ss.hasPermi('data:consultation:edit')")
    @Log(title = "咨询", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Consultation consultation)
    {
        return toAjax(consultationService.updateConsultation(consultation));
    }

    /**
     * 删除咨询
     */
    @PreAuthorize("@ss.hasPermi('data:consultation:remove')")
    @Log(title = "咨询", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(consultationService.deleteConsultationByIds(ids));
    }
}
