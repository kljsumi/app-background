package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.TalentSupply;
import com.ruoyi.data.service.ITalentSupplyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 人才供应Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/talentSupply")
public class TalentSupplyController extends BaseController
{
    @Autowired
    private ITalentSupplyService talentSupplyService;

    /**
     * 查询人才供应列表
     */
    @PreAuthorize("@ss.hasPermi('data:talentSupply:list')")
    @GetMapping("/list")
    public TableDataInfo list(TalentSupply talentSupply)
    {
        startPage();
        List<TalentSupply> list = talentSupplyService.selectTalentSupplyList(talentSupply);
        return getDataTable(list);
    }

    /**
     * 导出人才供应列表
     */
    @PreAuthorize("@ss.hasPermi('data:talentSupply:export')")
    @Log(title = "人才供应", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TalentSupply talentSupply)
    {
        List<TalentSupply> list = talentSupplyService.selectTalentSupplyList(talentSupply);
        ExcelUtil<TalentSupply> util = new ExcelUtil<TalentSupply>(TalentSupply.class);
        return util.exportExcel(list, "人才供应数据");
    }

    /**
     * 获取人才供应详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:talentSupply:query')")
    @GetMapping(value = "/{talentSupplyId}")
    public AjaxResult getInfo(@PathVariable("talentSupplyId") Long talentSupplyId)
    {
        return AjaxResult.success(talentSupplyService.selectTalentSupplyByTalentSupplyId(talentSupplyId));
    }

    /**
     * 新增人才供应
     */
    @PreAuthorize("@ss.hasPermi('data:talentSupply:add')")
    @Log(title = "人才供应", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TalentSupply talentSupply)
    {
        return toAjax(talentSupplyService.insertTalentSupply(talentSupply));
    }

    /**
     * 修改人才供应
     */
    @PreAuthorize("@ss.hasPermi('data:talentSupply:edit')")
    @Log(title = "人才供应", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TalentSupply talentSupply)
    {
        return toAjax(talentSupplyService.updateTalentSupply(talentSupply));
    }

    /**
     * 删除人才供应
     */
    @PreAuthorize("@ss.hasPermi('data:talentSupply:remove')")
    @Log(title = "人才供应", businessType = BusinessType.DELETE)
	@DeleteMapping("/{talentSupplyIds}")
    public AjaxResult remove(@PathVariable Long[] talentSupplyIds)
    {
        return toAjax(talentSupplyService.deleteTalentSupplyByTalentSupplyIds(talentSupplyIds));
    }
}
