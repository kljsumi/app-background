package com.ruoyi.data.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.data.service.IAllIndexService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.Policy;
import com.ruoyi.data.service.IPolicyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 政策Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/policy")
public class PolicyController extends BaseController
{
    @Autowired
    private IPolicyService policyService;

    @Autowired
    private IAllIndexService allIndexService;

    public static final String POLICY_CATEGORY = "政策分类";

    /**
     * 查询政策列表
     */
    @PreAuthorize("@ss.hasPermi('data:policy:list')")
    @GetMapping("/list")
    public TableDataInfo list(Policy policy)
    {
        startPage();
        List<Policy> list = policyService.selectPolicyList(policy);
        return getDataTable(list);
    }

    /**
     * 导出政策列表
     */
    @PreAuthorize("@ss.hasPermi('data:policy:export')")
    @Log(title = "政策", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Policy policy)
    {
        List<Policy> list = policyService.selectPolicyList(policy);
        ExcelUtil<Policy> util = new ExcelUtil<Policy>(Policy.class);
        return util.exportExcel(list, "政策数据");
    }

    /**
     * 获取政策详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:policy:query')")
    @GetMapping(value = {"/","/{policyId}"})
    public AjaxResult getInfo(@PathVariable(value = "policyId",required = false) Long policyId)
    {
        AjaxResult ajax = AjaxResult.success();

        ajax.put("category", allIndexService.getIndexByListName(POLICY_CATEGORY));
        if (StringUtils.isNotNull(policyId))
        {
            Policy policy = policyService.selectPolicyByPolicyId(policyId);
            ajax.put(AjaxResult.DATA_TAG, policy);
            ajax.put("category_u", policy.getCategory());
        }
        return ajax;
    }

    /**
     * 新增政策
     */
    @PreAuthorize("@ss.hasPermi('data:policy:add')")
    @Log(title = "政策", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Policy policy)
    {
        return toAjax(policyService.insertPolicy(policy));
    }

    /**
     * 修改政策
     */
    @PreAuthorize("@ss.hasPermi('data:policy:edit')")
    @Log(title = "政策", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Policy policy)
    {
        return toAjax(policyService.updatePolicy(policy));
    }

    /**
     * 删除政策
     */
    @PreAuthorize("@ss.hasPermi('data:policy:remove')")
    @Log(title = "政策", businessType = BusinessType.DELETE)
	@DeleteMapping("/{policyIds}")
    public AjaxResult remove(@PathVariable Long[] policyIds)
    {
        return toAjax(policyService.deletePolicyByPolicyIds(policyIds));
    }
}
