package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.Park;
import com.ruoyi.data.service.IParkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 科技园区Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/park")
public class ParkController extends BaseController
{
    @Autowired
    private IParkService parkService;

    /**
     * 查询科技园区列表
     */
    @PreAuthorize("@ss.hasPermi('data:park:list')")
    @GetMapping("/list")
    public TableDataInfo list(Park park)
    {
        startPage();
        List<Park> list = parkService.selectParkList(park);
        return getDataTable(list);
    }

    /**
     * 导出科技园区列表
     */
    @PreAuthorize("@ss.hasPermi('data:park:export')")
    @Log(title = "科技园区", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Park park)
    {
        List<Park> list = parkService.selectParkList(park);
        ExcelUtil<Park> util = new ExcelUtil<Park>(Park.class);
        return util.exportExcel(list, "科技园区数据");
    }

    /**
     * 获取科技园区详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:park:query')")
    @GetMapping(value = "/{parkId}")
    public AjaxResult getInfo(@PathVariable("parkId") Long parkId)
    {
        return AjaxResult.success(parkService.selectParkByParkId(parkId));
    }

    /**
     * 新增科技园区
     */
    @PreAuthorize("@ss.hasPermi('data:park:add')")
    @Log(title = "科技园区", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Park park)
    {
        return toAjax(parkService.insertPark(park));
    }

    /**
     * 修改科技园区
     */
    @PreAuthorize("@ss.hasPermi('data:park:edit')")
    @Log(title = "科技园区", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Park park)
    {
        return toAjax(parkService.updatePark(park));
    }

    /**
     * 删除科技园区
     */
    @PreAuthorize("@ss.hasPermi('data:park:remove')")
    @Log(title = "科技园区", businessType = BusinessType.DELETE)
	@DeleteMapping("/{parkIds}")
    public AjaxResult remove(@PathVariable Long[] parkIds)
    {
        return toAjax(parkService.deleteParkByParkIds(parkIds));
    }
}
