package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.FinancingNeed;
import com.ruoyi.data.service.IFinancingNeedService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 融资需求Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/financingNeed")
public class FinancingNeedController extends BaseController
{
    @Autowired
    private IFinancingNeedService financingNeedService;

    /**
     * 查询融资需求列表
     */
    @PreAuthorize("@ss.hasPermi('data:financingNeed:list')")
    @GetMapping("/list")
    public TableDataInfo list(FinancingNeed financingNeed)
    {
        startPage();
        List<FinancingNeed> list = financingNeedService.selectFinancingNeedList(financingNeed);
        return getDataTable(list);
    }

    /**
     * 导出融资需求列表
     */
    @PreAuthorize("@ss.hasPermi('data:financingNeed:export')")
    @Log(title = "融资需求", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FinancingNeed financingNeed)
    {
        List<FinancingNeed> list = financingNeedService.selectFinancingNeedList(financingNeed);
        ExcelUtil<FinancingNeed> util = new ExcelUtil<FinancingNeed>(FinancingNeed.class);
        return util.exportExcel(list, "融资需求数据");
    }

    /**
     * 获取融资需求详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:financingNeed:query')")
    @GetMapping(value = "/{financingNeedId}")
    public AjaxResult getInfo(@PathVariable("financingNeedId") Long financingNeedId)
    {
        return AjaxResult.success(financingNeedService.selectFinancingNeedByFinancingNeedId(financingNeedId));
    }

    /**
     * 新增融资需求
     */
    @PreAuthorize("@ss.hasPermi('data:financingNeed:add')")
    @Log(title = "融资需求", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FinancingNeed financingNeed)
    {
        return toAjax(financingNeedService.insertFinancingNeed(financingNeed));
    }

    /**
     * 修改融资需求
     */
    @PreAuthorize("@ss.hasPermi('data:financingNeed:edit')")
    @Log(title = "融资需求", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FinancingNeed financingNeed)
    {
        return toAjax(financingNeedService.updateFinancingNeed(financingNeed));
    }

    /**
     * 删除融资需求
     */
    @PreAuthorize("@ss.hasPermi('data:financingNeed:remove')")
    @Log(title = "融资需求", businessType = BusinessType.DELETE)
	@DeleteMapping("/{financingNeedIds}")
    public AjaxResult remove(@PathVariable Long[] financingNeedIds)
    {
        return toAjax(financingNeedService.deleteFinancingNeedByFinancingNeedIds(financingNeedIds));
    }
}
