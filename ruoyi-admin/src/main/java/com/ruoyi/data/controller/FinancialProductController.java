package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.FinancialProduct;
import com.ruoyi.data.service.IFinancialProductService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 金融产品Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/financialProduct")
public class FinancialProductController extends BaseController
{
    @Autowired
    private IFinancialProductService financialProductService;

    /**
     * 查询金融产品列表
     */
    @PreAuthorize("@ss.hasPermi('data:financialProduct:list')")
    @GetMapping("/list")
    public TableDataInfo list(FinancialProduct financialProduct)
    {
        startPage();
        List<FinancialProduct> list = financialProductService.selectFinancialProductList(financialProduct);
        return getDataTable(list);
    }

    /**
     * 导出金融产品列表
     */
    @PreAuthorize("@ss.hasPermi('data:financialProduct:export')")
    @Log(title = "金融产品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FinancialProduct financialProduct)
    {
        List<FinancialProduct> list = financialProductService.selectFinancialProductList(financialProduct);
        ExcelUtil<FinancialProduct> util = new ExcelUtil<FinancialProduct>(FinancialProduct.class);
        return util.exportExcel(list, "金融产品数据");
    }

    /**
     * 获取金融产品详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:financialProduct:query')")
    @GetMapping(value = "/{financialProductId}")
    public AjaxResult getInfo(@PathVariable("financialProductId") Long financialProductId)
    {
        return AjaxResult.success(financialProductService.selectFinancialProductByFinancialProductId(financialProductId));
    }

    /**
     * 新增金融产品
     */
    @PreAuthorize("@ss.hasPermi('data:financialProduct:add')")
    @Log(title = "金融产品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FinancialProduct financialProduct)
    {
        return toAjax(financialProductService.insertFinancialProduct(financialProduct));
    }

    /**
     * 修改金融产品
     */
    @PreAuthorize("@ss.hasPermi('data:financialProduct:edit')")
    @Log(title = "金融产品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FinancialProduct financialProduct)
    {
        return toAjax(financialProductService.updateFinancialProduct(financialProduct));
    }

    /**
     * 删除金融产品
     */
    @PreAuthorize("@ss.hasPermi('data:financialProduct:remove')")
    @Log(title = "金融产品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{financialProductIds}")
    public AjaxResult remove(@PathVariable Long[] financialProductIds)
    {
        return toAjax(financialProductService.deleteFinancialProductByFinancialProductIds(financialProductIds));
    }
}
