package com.ruoyi.data.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.data.domain.TechnologyAchievement;
import com.ruoyi.data.service.ITechnologyAchievementService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 技术成果Controller
 * 
 * @author ruoyi
 * @date 2021-09-30
 */
@RestController
@RequestMapping("/data/technologyAchievement")
public class TechnologyAchievementController extends BaseController
{
    @Autowired
    private ITechnologyAchievementService technologyAchievementService;

    /**
     * 查询技术成果列表
     */
    @PreAuthorize("@ss.hasPermi('data:technologyAchievement:list')")
    @GetMapping("/list")
    public TableDataInfo list(TechnologyAchievement technologyAchievement)
    {
        startPage();
        List<TechnologyAchievement> list = technologyAchievementService.selectTechnologyAchievementList(technologyAchievement);
        return getDataTable(list);
    }

    /**
     * 导出技术成果列表
     */
    @PreAuthorize("@ss.hasPermi('data:technologyAchievement:export')")
    @Log(title = "技术成果", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TechnologyAchievement technologyAchievement)
    {
        List<TechnologyAchievement> list = technologyAchievementService.selectTechnologyAchievementList(technologyAchievement);
        ExcelUtil<TechnologyAchievement> util = new ExcelUtil<TechnologyAchievement>(TechnologyAchievement.class);
        return util.exportExcel(list, "技术成果数据");
    }

    /**
     * 获取技术成果详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:technologyAchievement:query')")
    @GetMapping(value = "/{technologyAchievementId}")
    public AjaxResult getInfo(@PathVariable("technologyAchievementId") Long technologyAchievementId)
    {
        return AjaxResult.success(technologyAchievementService.selectTechnologyAchievementByTechnologyAchievementId(technologyAchievementId));
    }

    /**
     * 新增技术成果
     */
    @PreAuthorize("@ss.hasPermi('data:technologyAchievement:add')")
    @Log(title = "技术成果", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TechnologyAchievement technologyAchievement)
    {
        return toAjax(technologyAchievementService.insertTechnologyAchievement(technologyAchievement));
    }

    /**
     * 修改技术成果
     */
    @PreAuthorize("@ss.hasPermi('data:technologyAchievement:edit')")
    @Log(title = "技术成果", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TechnologyAchievement technologyAchievement)
    {
        return toAjax(technologyAchievementService.updateTechnologyAchievement(technologyAchievement));
    }

    /**
     * 删除技术成果
     */
    @PreAuthorize("@ss.hasPermi('data:technologyAchievement:remove')")
    @Log(title = "技术成果", businessType = BusinessType.DELETE)
	@DeleteMapping("/{technologyAchievementIds}")
    public AjaxResult remove(@PathVariable Long[] technologyAchievementIds)
    {
        return toAjax(technologyAchievementService.deleteTechnologyAchievementByTechnologyAchievementIds(technologyAchievementIds));
    }
}
