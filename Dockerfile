FROM java:8

EXPOSE 10001

ADD ruoyi-admin.jar app.jar
RUN bash -c 'touch /app.jar'

ENTRYPOINT ["java", "-jar", "app.jar", "--spring.profiles.active=pro"]